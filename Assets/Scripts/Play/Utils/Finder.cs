using Play;
using Random = System.Random;

namespace Harmony
{
    // Author : Zachary Derosby-Therrien
    public static partial class Finder
    {
        private static Random random;
        private static ItemValues itemValues;
        private static SceneBundleLoader findableSceneBundleLoader;
        
        public static Random Random => random ?? (random = new Random());
        
        public static ItemValues ItemValues => itemValues ?? (itemValues = new ItemValues());
        
        public static SceneBundleLoader SceneBundleLoader
        {
            get
            {
                if (!findableSceneBundleLoader)
                    findableSceneBundleLoader = FindWithTag<SceneBundleLoader>(Tags.MainController);

                return findableSceneBundleLoader;
            }
        }
    }
}