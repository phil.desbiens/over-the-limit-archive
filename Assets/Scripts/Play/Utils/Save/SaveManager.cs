﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public static class SaveManager
    {
        private const string SAVES_PATH = "/saves";

        public static void Save(string saveName, SaveData saveData)
        {
            var dataPath = Application.persistentDataPath + SAVES_PATH + "/";

            if (!Directory.Exists(dataPath))
                Directory.CreateDirectory(Application.persistentDataPath + SAVES_PATH);

            var serializer = new XmlSerializer(typeof(SaveData));
            var fileStream = new FileStream(dataPath + saveName, FileMode.Create);

            serializer.Serialize(fileStream, saveData);
            fileStream.Close();
        }

        public static SaveData LoadSave(string path)
        {
            var dataPath = Application.persistentDataPath + path;

            if (!File.Exists(dataPath))
                throw new FileNotFoundException();

            var serializer = new XmlSerializer(typeof(SaveData));
            var fileStream = new FileStream(dataPath, FileMode.Open);
            try
            {
                var save = serializer.Deserialize(fileStream) as SaveData;
                fileStream.Close();
                return save;
            }
            catch
            {
                fileStream.Close();
                throw new FileNotFoundException();
            }
        }

        public static void DeleteSave(string path)
        {
            var dataPath = Application.persistentDataPath + path;
            if (File.Exists(dataPath))
            {
                File.Delete(dataPath);
                SaveData.Current = new SaveData();
            }
        }
    }
}