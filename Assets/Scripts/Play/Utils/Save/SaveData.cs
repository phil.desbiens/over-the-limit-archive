﻿using UnityEngine.Serialization;

namespace Play
{
    [System.Serializable]
    // Author : Gabriel Fortier
    public class SaveData
    {
        public const string WEEB_ARSENAL_ACHIEVEMENT_NAME = "Weeb Arsenal";
        public const string HIGH_ON_PURCHASE_NAME = "High on purchase";
        public const string CAPITAL_TWO_ACHIEVEMENT_NAME = "Capital Two? Yep!";
        public const string RED_CARD_ACHIEVEMENT_NAME = "Red Card!";
        public const string SAFE_RIDE_ACHIEVEMENT_NAME = "Safe Ride";
        public const string BAGUETTE_ACHIEVEMENT_NAME = "Will you finish that baguette?";
        public const string WHILE_HOT_ACHIEVEMENT_NAME = "Get it while it's hot!";
        public const string POTION_SELLER_ACHIEVEMENT_NAME = "Potion Seller, I'm Going Into Battle!";

        private static SaveData current;

        public static SaveData Current
        {
            get => current ?? (current = new SaveData());
            set
            {
                if (value != null)
                {
                    current = value;
                }
            }
        }
        
        public string saveName;
        [FormerlySerializedAs("highestLevelUnlocked")] public LevelsEnum highestLevelsUnlocked = LevelsEnum.LEVEL_TUTORIAL;
        public int[] levelHighScore = {0, 0, 0, 0, 0};
        public bool[] achievements = {false, false, false, false, false, false, false, false};
    }
}