﻿namespace Play
{
    // Author : Guillaume Thivierge
    public static class Globals
    {
        public const float REVERSE_PLATFORM_EFFECTOR_WAY = 180f;
        public const float NORMAL_PLATFORM_EFFECTOR_WAY = 0f;

        // Author : Gabriel Fortier
        public const string PATH_SAVE_ONE = "/saves/SaveOne.save";
        public const string PATH_SAVE_TWO = "/saves/SaveTwo.save";
        public const string PATH_SAVE_THREE = "/saves/SaveThree.save";
    }
}