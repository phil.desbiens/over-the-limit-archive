﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class SceneInstantiation : MonoBehaviour
    {
        [SerializeField] private GameObject scenePrefab;
        [SerializeField] private int distanceBetweenScenes;
        [SerializeField] private GameObject mainCameraTwo;

        private void Start()
        {
            var obj = Instantiate(scenePrefab, new Vector3(0, 0, 0), Quaternion.identity);
            obj.transform.parent = transform;

            if (Finder.Main.GameMode != GameModesEnum.PVP) return;
            if (GameObject.FindWithTag(Tags.SecondCamera) == null)
            {
                Instantiate(mainCameraTwo, transform, true);
            }

            obj = Instantiate(scenePrefab,
                new Vector3(obj.gameObject.transform.position.x + distanceBetweenScenes,
                    obj.transform.position.y, 0), Quaternion.identity);

            obj.transform.parent = transform;
        }
    }
}