﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class GlobalLightInstantiation : MonoBehaviour
    {
        [SerializeField] private GameObject globalLight;

        private void Start()
        {
            if (GameObject.FindWithTag(Tags.Light) == null) Instantiate(globalLight, transform, true);
        }
    }
}