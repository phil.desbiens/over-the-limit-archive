﻿using Cinemachine;
using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Play
{
    // Author : Gabriel Fortier
    public class VirtualCameraInstantiation : MonoBehaviour
    {
        [SerializeField] private int distanceBetweenScenes;
        [SerializeField] private GameObject followingCameraPlayer1;
        [SerializeField] private GameObject followingCameraPlayer2;
        [SerializeField] private GameObject followingCameraSolo;
        [SerializeField] private GameObject cameraBound;
        
        private readonly Rect solo = new Rect(0, 0, 1, 1);
        private readonly Rect pvp = new Rect(0, 0.5f, 1, 0.5f);

        private GameObject mainCamera;

        private void Start()
        {
            var player1 = GameObject.FindWithTag(Tags.Player).GetComponent<PlayerInput>();

            var gameMode = Finder.Main.GameMode;
            var objCameraBound = Instantiate(cameraBound, transform, true);
            mainCamera = GameObject.FindWithTag(Tags.MainCamera);


            if (gameMode == GameModesEnum.SOLO)
            {
                mainCamera.GetComponent<Camera>().rect = solo;
                InstantiateVirtualCameraSolo(player1, objCameraBound);
            }

            if (gameMode == GameModesEnum.PVP)
            {
                var player2 = GameObject.FindWithTag(Tags.Player2);
                mainCamera.GetComponent<Camera>().rect = pvp;
                InstantiateVirtualCameraMulti(player1, objCameraBound, player2);
            }
        }

        private void OnDestroy()
        {
            if (mainCamera != null)
                mainCamera.GetComponent<Camera>().rect = solo;
        }

        private void InstantiateVirtualCameraSolo(PlayerInput player, GameObject bound)
        {
            var objCameraSolo = Instantiate(followingCameraSolo, transform, true);
            objCameraSolo.gameObject.GetComponent<CinemachineVirtualCamera>().Follow = player.transform;
            objCameraSolo.gameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D =
                bound.GetComponent<Collider2D>();
        }

        private void InstantiateVirtualCameraMulti(PlayerInput player, GameObject bound, GameObject player2)
        {
            var objCamera = Instantiate(followingCameraPlayer1, transform, true);
            objCamera.gameObject.GetComponent<CinemachineVirtualCamera>().Follow = player.transform;

            objCamera.gameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D =
                bound.GetComponent<Collider2D>();

            var objCamera2 = Instantiate(followingCameraPlayer2, transform, true);
            objCamera2.gameObject.GetComponent<CinemachineVirtualCamera>().Follow = player2.transform;

            var objBound2 = Instantiate(cameraBound, transform, true);
            var position = bound.transform.position;

            objBound2.transform.position =
                new Vector3(position.x + distanceBetweenScenes, position.y, 0);

            objCamera2.gameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D =
                objBound2.GetComponent<Collider2D>();
        }
    }
}