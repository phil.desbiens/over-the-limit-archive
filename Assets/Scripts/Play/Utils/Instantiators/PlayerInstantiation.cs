using System;
using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Play
{
    // Author : Gabriel Fortier
    public class PlayerInstantiation : MonoBehaviour
    {
        //Control scheme of player one when pvp, when solo: "Solo" and the control scheme of player two is always: "Player2" 
        [SerializeField] private int distanceBetweenScenes;
        [SerializeField] private GameObject player;
        [SerializeField] private GameObject player2;

        private const string PLAYER_1_PVP_CONTROL_SCHEME = "Player1";

        private void Start()
        {
            var gameMode = Finder.Main.GameMode;
            
            switch (gameMode)
            {
                case GameModesEnum.SOLO:
                {
                    var objPlayer = PlayerInput.Instantiate(player);
                    objPlayer.transform.parent = transform;
                    break;
                }
                case GameModesEnum.PVP:
                {
                    var objPlayer = PlayerInput.Instantiate(player, pairWithDevice: Keyboard.current,
                        controlScheme: PLAYER_1_PVP_CONTROL_SCHEME);
                    objPlayer.transform.parent = transform;

                    var objPlayer2 = PlayerInput.Instantiate(player2, pairWithDevice: Gamepad.current);
                    objPlayer2.transform.parent = transform;
                    var position = player.transform.position;

                    // We want to modify the position of the item, using a variable would prevent that or add unnecessary code
                    // ReSharper disable once Unity.InefficientPropertyAccess
                    objPlayer2.transform.position = new Vector3(position.x + distanceBetweenScenes, position.y, 0);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}