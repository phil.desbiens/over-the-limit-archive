﻿using Harmony;
using UnityEngine;

namespace Play
{
    public class GhostInstantiation : MonoBehaviour
    {
        public GameObject targets1;
        public GameObject targets2;
        public GameObject ghost1;
        public GameObject ghost2;

        private void Start()
        {
            var mode = Finder.Main.GameMode;

            Instantiate(targets1, transform, true);
            Instantiate(ghost1, transform, true);

            if (mode == GameModesEnum.PVP)
            {
                Instantiate(targets2, transform, true);
                Instantiate(ghost2, transform, true);
            }
        }
    }
}