﻿using UnityEngine;
using UnityEngine.Audio;

namespace Play
{
    // Author : Guillaume Thivierge
    //Inspired from https://gamedevbeginner.com/the-right-way-to-make-a-volume-slider-in-unity-using-logarithmic-conversion/
    public class SetVolume : MonoBehaviour
    {
        [SerializeField] AudioMixer mixer;

        public void SetSoundLevel(float sliderSoundValue)
        {
            mixer.SetFloat("SoundVolume", Mathf.Log10(sliderSoundValue) * 20);
        }
    }
}