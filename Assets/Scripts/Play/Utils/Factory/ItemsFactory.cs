﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Play
{
    [Findable(Tags.GameController)]
    // Author : Zachary Derosby-Therrien
    public class ItemsFactory : MonoBehaviour
    {
        [Tooltip("Position where inactive items are placed")] [SerializeField]
        private Vector3 bufferPosition = new Vector3(0, 0, 0);

        [SerializeField] private List<ItemEntity> itemPrefabs;
        private readonly List<ItemEntity> allCreatedItems = new List<ItemEntity>();

        private ItemEntity CreateInstance(ItemNameEnum itemName)
        {
            return (from itemsPrefab in itemPrefabs
                where itemsPrefab.GetComponent<ItemEntity>().Name.Equals(itemName)
                select Instantiate(itemsPrefab)).FirstOrDefault();
        }

        private ItemEntity GenerateNewItem(ItemNameEnum itemName)
        {
            var item = CreateInstance(itemName);
            allCreatedItems.Add(item);
            return item;
        }

        public ItemEntity GetItemToSpawn(ItemNameEnum itemName)
        {
            var itemFound = allCreatedItems.FirstOrDefault(item => item.Name.Equals(itemName) && !item.IsAlive);

            if (itemFound == null)
            {
                itemFound = GenerateNewItem(itemName);
                RecycleItem(itemFound);
            }

            return itemFound;
        }

        public void RecycleItem(ItemEntity item)
        {
            item.transform.parent = transform;

            // We want to modify the position of the item, using a variable would prevent that or add unnecessary code
            // ReSharper disable once Unity.InefficientPropertyAccess
            item.transform.position = bufferPosition;
        }
    }
}