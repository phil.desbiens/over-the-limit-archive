﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace Play
{
    // Author : Gabriel Fortier
    public class Level3LightManager : MonoBehaviour
    {
        [Tooltip("Amount of seconds")] [SerializeField]
        private short lightTime = 5;

        [Tooltip("Amount of seconds")] [SerializeField]
        private short darkTime = 15;

        [Tooltip("Intensity of the light when its dark time")] [SerializeField]
        private float darkTimeIntensity = 0.06f;

        [Tooltip("Intensity of the light when its light time")] [SerializeField]
        private float lightTimeIntensity = 0.6f;

        private Light2D GlobalLight { get; set; }

        private void Awake()
        {
            GlobalLight = GetComponent<Light2D>();
            StartCoroutine(LightsOn());
        }

        private IEnumerator LightsOn()
        {
            GlobalLight.intensity = lightTimeIntensity;
            yield return new WaitForSeconds(lightTime);
            StartCoroutine(LightsOff());
        }

        private IEnumerator LightsOff()
        {
            GlobalLight.intensity = darkTimeIntensity;
            yield return new WaitForSeconds(darkTime);
            StartCoroutine(LightsOn());
        }

        public bool LevelIsInDark()
        {
            return Math.Abs(GlobalLight.intensity - darkTimeIntensity) < 0.01f;
        }
    }
}