﻿using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public static class TimeManager
    {
        public static void PauseGame()
        {
            Time.timeScale = 0;
        }

        public static void ResumeGame()
        {
            Time.timeScale = 1;
        }
    }
}