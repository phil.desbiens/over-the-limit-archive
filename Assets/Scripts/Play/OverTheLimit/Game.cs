using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Play
{
    [Findable(Tags.GameController)]
    // Author : Zachary Derosby-Therrien
    public class Game : MonoBehaviour
    {
        [SerializeField] private List<SceneBundle> allScenes;
        [SerializeField] private SceneBundle introLevelScreen;
        [SerializeField] private SceneBundle endLevelScreen;
        [SerializeField] private SceneBundle multiplayerEndLevelScreen;
        [SerializeField] private SceneBundle uiScene;
        [SerializeField] [Min(1)] private float defaultTime = 25f;
        [SerializeField] private int spendingGoal;

        private Main main;
        private SceneBundleLoader loader;
        private AchievementGetEventChannel achievementGetEventChannel;
        private ShoppingCart[] shoppingCarts;

        private bool hasPickedUpItem;
        private bool isInLevel;
        private int nbCashOuts;

        public float DefaultTime => defaultTime;
        public int SpendingGoal => spendingGoal;
        public bool IsLevelWon { get; private set; }
        public int HighestMoneySpent { get; private set; }
        public float TimeRemaining { get; private set; }
        public LevelsEnum CurrentLevel { get; set; }


        public ShoppingCart[] ShoppingCarts
        {
            get
            {
                if (shoppingCarts != null
                    && !shoppingCarts[0].IsDestroyed()
                    && shoppingCarts.Length > 0)
                {
                    return shoppingCarts;
                }

                var player1 = GameObject.FindGameObjectWithTag(Tags.Player);
                GameObject[] players;

                if (main.GameMode == GameModesEnum.PVP)
                {
                    var player2 = GameObject.FindGameObjectWithTag(Tags.Player2);
                    players = new[] {player1, player2};
                }
                else players = new[] {player1};

                var newShoppingCarts = new ShoppingCart[players.Length];

                for (int i = 0; i < players.Length; i++)
                {
                    newShoppingCarts[i] = players[i].GetComponent<ShoppingCart>();
                }

                shoppingCarts = newShoppingCarts;
                return shoppingCarts;
            }
        }


        private void Awake()
        {
            main = Finder.Main;
            loader = Finder.SceneBundleLoader;
            achievementGetEventChannel = Finder.AchievementGetEventChannel;
            IsLevelWon = false;
            HighestMoneySpent = 0;
            LoadIntroLevelScreen();
        }

        private void Update()
        {
            if (isInLevel)
            {
                if (TimeRemaining > 0)
                {
                    TimeRemaining -= Time.deltaTime;

                    if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.SAFE_RIDE))
                    {
                        HandleSafeRideAchievement();
                    }
                }

                else
                {
                    if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.BAGUETTE))
                    {
                        HandleBaguetteAchievement();
                    }

                    if (main.GameMode == GameModesEnum.SOLO)
                        WinningVerification();
                    else StartEndLevelMultiplayerCycle();
                }
            }
        }

        private void WinningVerification()
        {
            HighestMoneySpent = 0;
            foreach (var shoppingCart in ShoppingCarts)
            {
                if (shoppingCart.MoneySpent > HighestMoneySpent) HighestMoneySpent = shoppingCart.MoneySpent;
            }

            if (SpendingGoal < HighestMoneySpent) StartLevelWonCycle();
            else StartLevelLostCycle();
        }
        
        private void StartLevelWonCycle()
        {
            IsLevelWon = true;
            isInLevel = false;
            UpdateCurrentLevelHighScore();

            IEnumerator Routine()
            {
                yield return loader.Unload(uiScene);
                yield return loader.Load(endLevelScreen);
                yield return UnloadCurrentLevel();

                CurrentLevel++;

                UpdateMaxLevelAvailable();

                if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.CAPITAL_TWO))
                    HandleCapitalTwoAchievement();

                SaveManager.Save(SaveData.Current.saveName, SaveData.Current);
            }

            StartCoroutine(Routine());
        }

        // Author : Zachary Derosby-Therrien
        private void StartLevelLostCycle()
        {
            IsLevelWon = false;
            isInLevel = false;

            IEnumerator Routine()
            {
                yield return loader.Unload(uiScene);
                yield return loader.Load(endLevelScreen);
                yield return UnloadCurrentLevel();
            }

            StartCoroutine(Routine());
        }

        private void LoadIntroLevelScreen()
        {
            IEnumerator Routine()
            {
                yield return loader.Load(introLevelScreen);
            }

            StartCoroutine(Routine());
        }

        public void StartLevel()
        {
            IEnumerator Routine()
            {
                yield return loader.Unload(introLevelScreen);
                TimeRemaining = defaultTime;
                isInLevel = true;
                IsLevelWon = false;
                LoadLevel();
            }

            StartCoroutine(Routine());
        }

        // Author : Guillaume Thivierge
        private void LoadLevel()
        {
            IEnumerator Routine()
            {
                yield return loader.Load(allScenes[(int) CurrentLevel]);
                yield return loader.Load(uiScene);
            }

            StartCoroutine(Routine());
        }

        public void EndLevel()
        {
            if (main.GameMode == GameModesEnum.SOLO)
            {
                loader.Unload(endLevelScreen);

                if (CurrentLevel - 1 == LevelsEnum.LEVEL_FOUR) main.LoadGameEndScreen();

                else LoadIntroLevelScreen();
            }
            else
            {
                loader.Unload(multiplayerEndLevelScreen);
                UnloadUI();
                UnloadCurrentLevel();
                StartCoroutine(main.UnloadGameScene());
            }
        }

        private void StartEndLevelMultiplayerCycle()
        {
            IsLevelWon = false;
            isInLevel = false;

            IEnumerator Routine()
            {
                yield return loader.Unload(uiScene);
                yield return loader.Load(multiplayerEndLevelScreen);
                yield return UnloadCurrentLevel();
            }

            StartCoroutine(Routine());
        }

        // Author : Zachary Derosby-Therrien, Gabriel Fortier
        public void TogglePauseMenu()
        {
            if (!main.IsLoadedPauseScene
                && !main.IsLoadedOptionsScene
                && !main.IsLoadedAchievementsScene
                && allScenes[(int) CurrentLevel].IsLoaded)
            {
                TimeManager.PauseGame();
                main.LoadPauseScenes();
            }
            else
            {
                TimeManager.ResumeGame();
                main.UnloadCurrentScene();
            }
        }

        public Coroutine UnloadCurrentLevel()
        {
            return loader.Unload(allScenes[(int) CurrentLevel]);
        }

        public Coroutine UnloadUI()
        {
            return loader.Unload(uiScene);
        }

        private void UpdateCurrentLevelHighScore()
        {
            if (SaveData.Current.levelHighScore[(int) CurrentLevel] < HighestMoneySpent)
                SaveData.Current.levelHighScore[(int) CurrentLevel] = HighestMoneySpent;
        }

        private void UpdateMaxLevelAvailable()
        {
            if (CurrentLevel != LevelsEnum.GAME_END
                && CurrentLevel > SaveData.Current.highestLevelsUnlocked)
                SaveData.Current.highestLevelsUnlocked = CurrentLevel;
        }

        public void SetTimeRemaining(float timeRemaining)
        {
            TimeRemaining = timeRemaining;
        }

        public void ResetTimeToDefault()
        {
            TimeRemaining = defaultTime;
        }

        private void HandleCapitalTwoAchievement()
        {
            if (shoppingCarts[0].MoneySpent >= SpendingGoal * 2)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.CAPITAL_TWO] = true;
                achievementGetEventChannel.Publish(SaveData.CAPITAL_TWO_ACHIEVEMENT_NAME);
            }
        }

        private void HandleBaguetteAchievement()
        {
            if (shoppingCarts[0].HasBoughtBaguetteOnly && shoppingCarts[0].MoneySpent > 0)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.BAGUETTE] = true;
                achievementGetEventChannel.Publish(SaveData.BAGUETTE_ACHIEVEMENT_NAME);
            }
        }

        private void HandleSafeRideAchievement()
        {
            if (shoppingCarts != null
                && shoppingCarts[0].ShoppingCartValue != 0
                && !hasPickedUpItem)
            {
                hasPickedUpItem = true;
            }
            else if (shoppingCarts != null
                     && hasPickedUpItem
                     && shoppingCarts[0].ShoppingCartValue == 0)
            {
                nbCashOuts++;
                hasPickedUpItem = false;
            }

            if (nbCashOuts == 4)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.SAFE_RIDE] = true;
                achievementGetEventChannel.Publish(SaveData.SAFE_RIDE_ACHIEVEMENT_NAME);
            }
        }
    }
}