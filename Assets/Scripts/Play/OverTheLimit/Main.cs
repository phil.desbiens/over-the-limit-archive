using System.Collections;
using DG.Tweening;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier & Zachary Derosby-Therrien
    [Findable(Tags.MainController)]
    public class Main : MonoBehaviour
    {
        [Header("Scenes")] [SerializeField] private SceneBundle homeScenes;
        [SerializeField] private SceneBundle gameScenes;
        [SerializeField] private SceneBundle levelSelectScenes;
        [SerializeField] private SceneBundle pauseScenes;
        [SerializeField] private SceneBundle optionScenes;
        [SerializeField] private SceneBundle saveScene;
        [SerializeField] private SceneBundle newSaveScene;
        [SerializeField] private SceneBundle gameEndScene;
        [SerializeField] private SceneBundle achievementsScene;

        private const string MENU_MUSIC = "MenuMusic";
        private const string BUYING_SOUND = "BuyingSound";
        private const string END_LEVEL_SOUND = "EndLevelSound";
        private const string RUNNING_SOUND = "RunningSound";
        private const string BUTTON_SOUND = "ButtonSound";
        private const string ITEM_PICKUP_SOUND = "ItemPickupSound";
        private const string JUMP_SOUND = "JumpSound";

        private SceneBundleLoader loader;
        private MenuStacker menuStacker;

        // Author : Guillaume Thivierge
        public AudioSource MenuMusic { get; private set; }
        public AudioSource BuyingSound { get; private set; }
        public AudioSource EndLevelSound { get; private set; }
        public AudioSource RunningSound { get; private set; }
        public AudioSource JumpSound { get; private set; }
        public AudioSource MenuButtonSound { get; private set; }
        public AudioSource PickUpSound { get; private set; }

        public GameModesEnum GameMode { get; set; } = GameModesEnum.SOLO;


        // Author : Zachary Derosby-Therrien
        public bool IsLoadedPauseScene => pauseScenes.IsLoaded;

        // Author  : Gabriel Fortier
        public bool IsLoadedOptionsScene => optionScenes.IsLoaded;

        // Author : Guillaume Thivierge
        public bool IsLoadedAchievementsScene => achievementsScene.IsLoaded;

        private void Awake()
        {
            loader = Finder.SceneBundleLoader;
            menuStacker = GetComponent<MenuStacker>();
            
            // Author : Guillaume Thivierge
            MenuMusic = GameObject.Find(MENU_MUSIC).GetComponent<AudioSource>();
            BuyingSound = GameObject.Find(BUYING_SOUND).GetComponent<AudioSource>();
            EndLevelSound = GameObject.Find(END_LEVEL_SOUND).GetComponent<AudioSource>();
            RunningSound = GameObject.Find(RUNNING_SOUND).GetComponent<AudioSource>();
            MenuButtonSound = GameObject.Find(BUTTON_SOUND).GetComponent<AudioSource>();
            
            // Author : Philippe Desbiens
            JumpSound = GameObject.Find(JUMP_SOUND).GetComponent<AudioSource>();

            // Author : Kaylen Bélanger
            PickUpSound = GameObject.Find(ITEM_PICKUP_SOUND).GetComponent<AudioSource>();

            DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
            DOTween.SetTweensCapacity(200, 125);
        }

        [UsedImplicitly]
        private void Start()
        {
#if UNITY_EDITOR
            if (gameScenes.IsLoaded) loader.Load(gameScenes);
            else
#endif
                LoadHomeScenes();
        }

        private void LoadHomeScenes()
        {
            menuStacker.LoadNewSceneOnTop(homeScenes);
        }

        public IEnumerator LoadGameScenes(LevelsEnum currentLevel)
        {
            MenuButtonSound.Play();
            yield return loader.Load(gameScenes);
            Finder.Game.CurrentLevel = currentLevel;
            yield return menuStacker.UnloadCurrentScene();
        }

        public IEnumerator UnloadGameScene()
        {
            yield return menuStacker.LoadCurrentScene();
            yield return loader.Unload(gameScenes);
        }

        public void LoadPauseScenes()
        {
            MenuButtonSound.Play();
            menuStacker.LoadNewSceneOnTop(pauseScenes);
        }

        public Coroutine LoadOptionsScenes()
        {
            MenuButtonSound.Play();
            return menuStacker.LoadNewSceneOnTop(optionScenes);
        }

        public Coroutine LoadSaveScene()
        {
            MenuButtonSound.Play();
            return menuStacker.LoadNewSceneOnTop(saveScene);
        }

        public Coroutine LoadNewSaveScene()
        {
            MenuButtonSound.Play();
            return menuStacker.LoadNewSceneOnTop(newSaveScene);
        }

        public Coroutine LoadLevelSelectScene()
        {
            MenuButtonSound.Play();
            return menuStacker.LoadNewSceneOnTop(levelSelectScenes);
        }

        // Author : Guillaume Thivierge
        public Coroutine LoadAchievementScenes()
        {
            MenuButtonSound.Play();
            return menuStacker.LoadNewSceneOnTop(achievementsScene);
        }

        // Author : Zachary Derosby-Therrien
        public void LoadGameEndScreen()
        {
            menuStacker.LoadNewSceneOnTop(gameEndScene);
        }

        public Coroutine ReturnPreviousScene()
        {
            MenuButtonSound.Play();
            return menuStacker.ReturnPreviousScene();
        }

        public Coroutine UnloadCurrentScene()
        {
            return menuStacker.UnloadCurrentScene();
        }

        public Coroutine ResetToMenuScene()
        {
            MenuButtonSound.Play();
            loader.Unload(gameScenes);
            return menuStacker.ReturnToFirstScene();
        }
    }
}