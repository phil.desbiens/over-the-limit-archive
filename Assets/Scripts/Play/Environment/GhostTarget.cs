﻿using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class GhostTarget : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            GhostEntity ghost = other.GetComponent<GhostEntity>();
            if (ghost != null) ghost.SwapTarget();
        }
    }
}