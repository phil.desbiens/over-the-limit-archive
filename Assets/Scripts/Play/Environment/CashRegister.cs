using UnityEngine;

namespace Play
{

    // Author : Guillaume Thivierge
    public class CashRegister : MonoBehaviour, IInteractable, IDisableable
    {
        private bool isDisabled;

        public void Enable()
        {
            isDisabled = false;
        }

        public void Disable()
        {
            isDisabled = true;
        }

        // Author : Philippe Desbiens
        public void Interact(PlayerController sender)
        {
            if (!isDisabled) sender.ShoppingCart.BuyItem();
        }
    }
}