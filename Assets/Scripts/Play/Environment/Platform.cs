using System.Collections;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class Platform : MonoBehaviour
    {
        [SerializeField] private float passThroughDelay = 0.4f;
        private PlatformEffector2D platformEffector2D;
        private BoxCollider2D boxCollider2D;

        public BoxCollider2D BoxCollider2D => boxCollider2D;

        private void Awake()
        {
            boxCollider2D = transform.GetComponent<BoxCollider2D>();
        }
    }
}