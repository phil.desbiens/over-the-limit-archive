﻿using System;
using TMPro;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class ItemPriceTag : MonoBehaviour
    {
        [SerializeField] private string amountFormat = "{0:0000}$";
        private TMP_Text priceText;
        private ItemEntity itemEntity;

        private void Awake()
        {
            itemEntity = GetComponent<ItemEntity>();
            priceText = GetComponentInChildren<TMP_Text>();

            priceText.text = String.Format(amountFormat, itemEntity.ItemValue.ToString());
        }

        public void ChangeTextToGreen()
        {
            if (!IsDiscountTag()) priceText.color = Color.green;
        }

        public void ChangeTextToDefault()
        {
            priceText.color = !IsDiscountTag() ? Color.white : Color.red;
        }

        private bool IsDiscountTag()
        {
            return itemEntity.Name == ItemNameEnum.DISCOUNT_TAG
                   || itemEntity.Name == ItemNameEnum.DISCOUNT_TAG_LEVEL_THREE;
        }
    }
}