﻿using System;
using TMPro;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class ClerkPriceTag : MonoBehaviour
    {
        [SerializeField] private string amountFormat = "{0:0000} $";

        private TMP_Text priceText;
        private ClerkEntity clerkEntity;

        private void Awake()
        {
            clerkEntity = GetComponent<ClerkEntity>();
            priceText = GetComponentInChildren<TMP_Text>();

            priceText.color = Color.red;
            priceText.text = String.Format(amountFormat, clerkEntity.AmountLimit.ToString());
        }
    }
}