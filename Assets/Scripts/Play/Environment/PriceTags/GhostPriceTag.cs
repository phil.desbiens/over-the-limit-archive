﻿using System;
using TMPro;
using UnityEngine;

namespace Play
{
    public class GhostPriceTag : MonoBehaviour
    {
        [SerializeField] private string amountFormat = "{0:0000} $";

        private TMP_Text priceText;
        private GhostEntity ghostEntity;

        private void Awake()
        {
            ghostEntity = GetComponent<GhostEntity>();
            priceText = GetComponentInChildren<TMP_Text>();

            priceText.color = Color.red;
            priceText.text = String.Format(amountFormat, ghostEntity.AmountLimit.ToString());
        }
    }
}