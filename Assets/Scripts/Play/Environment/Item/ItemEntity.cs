﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class ItemEntity : MonoBehaviour, IInteractable, IDisableable
    {
        [SerializeField] private new ItemNameEnum name;
        [SerializeField] private float valuePercentWhenDiscounted = 0.75f;

        private float itemLifespanInSecond;
        private bool isDisabled;

        private ItemValues itemValues;

        public ItemNameEnum Name => name;
        public bool IsAlive { get; private set; }
        public int ItemValue { get; private set; }
        public int CurrentValue { get; set; }
        
        private void Awake()
        {
            itemValues = Finder.ItemValues;
            IsAlive = false;

            SetInitialItemValue();
        }

        private IEnumerator ItemLifeCycle(float lifeSpan)
        {
            IsAlive = true;
            itemLifespanInSecond = lifeSpan;
            CurrentValue = ItemValue;

            while (IsAlive)
            {
                if (itemLifespanInSecond > 0)
                    itemLifespanInSecond -= 1;
                else if (itemLifespanInSecond <= 0 && IsAlive)
                    EndLifespan();
                yield return new WaitForSeconds(1);
            }
        }

        private void SetInitialItemValue()
        {
            switch (name)
            {
                case ItemNameEnum.DISCOUNT_TAG_LEVEL_THREE:
                    ItemValue = (int) ((1 - valuePercentWhenDiscounted) * 100);
                    break;
                case ItemNameEnum.DISCOUNT_TAG:
                    ItemValue = (int) ((1 - valuePercentWhenDiscounted) * 100);
                    break;
                case ItemNameEnum.BREAD:
                    ItemValue = itemValues.BreadValue;
                    break;
                case ItemNameEnum.COOKIE:
                    ItemValue = itemValues.CookieValue;
                    break;
                case ItemNameEnum.JAM:
                    ItemValue = itemValues.JamValue;
                    break;
                case ItemNameEnum.BROWNIE:
                    ItemValue = itemValues.BrownieValue;
                    break;
                case ItemNameEnum.PIE:
                    ItemValue = itemValues.PieValue;
                    break;
                case ItemNameEnum.JERKY:
                    ItemValue = itemValues.JerkyValue;
                    break;
                case ItemNameEnum.CHICKEN_LEG:
                    ItemValue = itemValues.ChickenLegValue;
                    break;
                case ItemNameEnum.RIBS:
                    ItemValue = itemValues.RibsValue;
                    break;
                case ItemNameEnum.BACON:
                    ItemValue = itemValues.BaconValue;
                    break;
                case ItemNameEnum.STEAK:
                    ItemValue = itemValues.SteakValue;
                    break;
                case ItemNameEnum.SHIELD:
                    ItemValue = itemValues.ShieldValue;
                    break;
                case ItemNameEnum.HELMET:
                    ItemValue = itemValues.HelmetValue;
                    break;
                case ItemNameEnum.GAUNTLET:
                    ItemValue = itemValues.GauntletValue;
                    break;
                case ItemNameEnum.RING:
                    ItemValue = itemValues.RingValue;
                    break;
                case ItemNameEnum.CHEST_PLATE:
                    ItemValue = itemValues.ChestPlateValue;
                    break;
                case ItemNameEnum.RED_POTION:
                    ItemValue = itemValues.RedPotionValue;
                    break;
                case ItemNameEnum.BLUE_POTION:
                    ItemValue = itemValues.BluePotionValue;
                    break;
                case ItemNameEnum.PURPLE_POTION:
                    ItemValue = itemValues.PurplePotionValue;
                    break;
                case ItemNameEnum.GREEN_POTION:
                    ItemValue = itemValues.GreenPotionValue;
                    break;
                case ItemNameEnum.WHITE_POTION:
                    ItemValue = itemValues.WhitePotionValue;
                    break;
                case ItemNameEnum.DAGGER:
                    ItemValue = itemValues.DaggerValue;
                    break;
                case ItemNameEnum.BOW:
                    ItemValue = itemValues.BowValue;
                    break;
                case ItemNameEnum.SWORD:
                    ItemValue = itemValues.SwordValue;
                    break;
                case ItemNameEnum.MACE:
                    ItemValue = itemValues.MaceValue;
                    break;
                case ItemNameEnum.BATTLEAXE:
                    ItemValue = itemValues.BattleAxeValue;
                    break;
            }
        }

        public void Disable()
        {
            isDisabled = true;
        }

        // Author : Guillaume Thivierge
        public void Enable()
        {
            isDisabled = false;
        }

        public void Interact(PlayerController sender)
        {
            if (isDisabled) return;
            itemLifespanInSecond = 0;
            IsAlive = false;
            sender.StartGrabbingProcess(this);
        }

        public void StartLifespan(float lifeSpan)
        {
            StartCoroutine(ItemLifeCycle(lifeSpan));
        }

        private void EndLifespan()
        {
            IsAlive = false;
        }

        public void ApplyDiscount()
        {
            CurrentValue = (int) (ItemValue * valuePercentWhenDiscounted);
        }
    }
}