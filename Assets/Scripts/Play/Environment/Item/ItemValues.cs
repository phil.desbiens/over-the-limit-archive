﻿using System;
using Harmony;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class ItemValues
    {
        private const int MINIMUM_PRICE_CATEGORY_1 = 300;
        private const int MAXIMUM_PRICE_CATEGORY_1 = 500;

        private const int MINIMUM_PRICE_CATEGORY_2 = 500;
        private const int MAXIMUM_PRICE_CATEGORY_2 = 700;

        private const int MINIMUM_PRICE_CATEGORY_3 = 700;
        private const int MAXIMUM_PRICE_CATEGORY_3 = 900;

        private const int MINIMUM_PRICE_CATEGORY_4 = 900;
        private const int MAXIMUM_PRICE_CATEGORY_4 = 1100;

        private static readonly Random Rnd = Finder.Random;

        private short baconValue;
        private short battleAxeValue;
        private short bluePotionValue;
        private short bowValue;

        private short breadValue;
        private short brownieValue;
        private short chestPlateValue;
        private short chickenLegValue;
        private short cookieValue;

        private short daggerValue;
        private short gauntletValue;
        private short greenPotionValue;
        private short helmetValue;
        private short jamValue;

        private short jerkyValue;
        private short maceValue;
        private short pieValue;
        private short purplePotionValue;

        private short redPotionValue;
        private short ribsValue;
        private short ringValue;

        private short shieldValue;
        private short steakValue;
        private short swordValue;
        private short whitePotionValue;

        public short BreadValue
        {
            get
            {
                if (breadValue == 0)
                    breadValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return breadValue;
            }
        }

        public short CookieValue
        {
            get
            {
                if (cookieValue == 0)
                    cookieValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return cookieValue;
            }
        }

        public short JamValue
        {
            get
            {
                if (jamValue == 0)
                    jamValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_2, MAXIMUM_PRICE_CATEGORY_2);

                return jamValue;
            }
        }

        public short BrownieValue
        {
            get
            {
                if (brownieValue == 0)
                    brownieValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_3, MAXIMUM_PRICE_CATEGORY_3);

                return brownieValue;
            }
        }

        public short PieValue
        {
            get
            {
                if (pieValue == 0)
                    pieValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_4, MAXIMUM_PRICE_CATEGORY_4);

                return pieValue;
            }
        }

        public short JerkyValue
        {
            get
            {
                if (jerkyValue == 0)
                    jerkyValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return jerkyValue;
            }
        }

        public short ChickenLegValue
        {
            get
            {
                if (chickenLegValue == 0)
                    chickenLegValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return chickenLegValue;
            }
        }

        public short RibsValue
        {
            get
            {
                if (ribsValue == 0)
                    ribsValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_2, MAXIMUM_PRICE_CATEGORY_2);

                return ribsValue;
            }
        }

        public short BaconValue
        {
            get
            {
                if (baconValue == 0)
                    baconValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_3, MAXIMUM_PRICE_CATEGORY_3);

                return baconValue;
            }
        }

        public short SteakValue
        {
            get
            {
                if (steakValue == 0)
                    steakValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_4, MAXIMUM_PRICE_CATEGORY_4);

                return steakValue;
            }
        }

        public short ShieldValue
        {
            get
            {
                if (shieldValue == 0)
                    shieldValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return shieldValue;
            }
        }

        public short HelmetValue
        {
            get
            {
                if (helmetValue == 0)
                    helmetValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return helmetValue;
            }
        }

        public short GauntletValue
        {
            get
            {
                if (gauntletValue == 0)
                    gauntletValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_2, MAXIMUM_PRICE_CATEGORY_2);

                return gauntletValue;
            }
        }

        public short RingValue
        {
            get
            {
                if (ringValue == 0)
                    ringValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_3, MAXIMUM_PRICE_CATEGORY_3);

                return ringValue;
            }
        }

        public short ChestPlateValue
        {
            get
            {
                if (chestPlateValue == 0)
                    chestPlateValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_4, MAXIMUM_PRICE_CATEGORY_4);

                return chestPlateValue;
            }
        }

        public short RedPotionValue
        {
            get
            {
                if (redPotionValue == 0)
                    redPotionValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return redPotionValue;
            }
        }

        public short BluePotionValue
        {
            get
            {
                if (bluePotionValue == 0)
                    bluePotionValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return bluePotionValue;
            }
        }

        public short PurplePotionValue
        {
            get
            {
                if (purplePotionValue == 0)
                    purplePotionValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_2, MAXIMUM_PRICE_CATEGORY_2);

                return purplePotionValue;
            }
        }

        public short GreenPotionValue
        {
            get
            {
                if (greenPotionValue == 0)
                    greenPotionValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_3, MAXIMUM_PRICE_CATEGORY_3);

                return greenPotionValue;
            }
        }

        public short WhitePotionValue
        {
            get
            {
                if (whitePotionValue == 0)
                    whitePotionValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_4, MAXIMUM_PRICE_CATEGORY_4);

                return whitePotionValue;
            }
        }

        public short DaggerValue
        {
            get
            {
                if (daggerValue == 0)
                    daggerValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return daggerValue;
            }
        }

        public short BowValue
        {
            get
            {
                if (bowValue == 0)
                    bowValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_1, MAXIMUM_PRICE_CATEGORY_1);

                return bowValue;
            }
        }

        public short SwordValue
        {
            get
            {
                if (swordValue == 0)
                    swordValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_2, MAXIMUM_PRICE_CATEGORY_2);

                return swordValue;
            }
        }

        public short MaceValue
        {
            get
            {
                if (maceValue == 0)
                    maceValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_3, MAXIMUM_PRICE_CATEGORY_3);

                return maceValue;
            }
        }

        public short BattleAxeValue
        {
            get
            {
                if (battleAxeValue == 0)
                    battleAxeValue = (short) Rnd.Next(MINIMUM_PRICE_CATEGORY_4, MAXIMUM_PRICE_CATEGORY_4);

                return battleAxeValue;
            }
        }
    }
}