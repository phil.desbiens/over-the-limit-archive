﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using Random = System.Random;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class ItemSpawner : MonoBehaviour
    {
        [SerializeField] private int minimumDowntime = 5;
        [SerializeField] private int maximumDowntime = 15;
        [SerializeField] private int spawnedItemsLifespan = 10;

        [Tooltip("Items pris en charge par le spawner")] [SerializeField]
        private List<ItemNameEnum> handledItems = new List<ItemNameEnum>();

        private ItemsFactory factory;
        private ItemEntity itemOnSpawner;
        private Random rnd;

        private void Awake()
        {
            factory = Finder.ItemsFactory;
            rnd = Finder.Random;
        }

        private void OnEnable()
        {
            StartSpawnCycle();
        }

        private void StartSpawnCycle()
        {
            StartCoroutine(SpawnCycle());
        }

        private IEnumerator SpawnCycle()
        {
            while (isActiveAndEnabled)
            {
                var spawnDowntime = rnd.Next(minimumDowntime, maximumDowntime);

                yield return new WaitForSeconds(spawnDowntime);

                SpawnItem();

                yield return new WaitUntil(() => itemOnSpawner != null && !itemOnSpawner.IsAlive);
                DespawnCurrentItemOnSpawner();
                itemOnSpawner = null;
            }
        }

        private void SpawnItem()
        {
            var randomNumber = rnd.Next(0, handledItems.Count);
            var spawnedItem = factory.GetItemToSpawn(handledItems[randomNumber]);

            itemOnSpawner = spawnedItem;
            var transformParent = transform;
            spawnedItem.transform.parent = transformParent;

            // We want to modify the position of the item, using a variable would prevent that or add unnecessary code
            // ReSharper disable once Unity.InefficientPropertyAccess
            spawnedItem.transform.position = transformParent.position;

            spawnedItem.StartLifespan(spawnedItemsLifespan);
        }

        private void DespawnCurrentItemOnSpawner()
        {
            factory.RecycleItem(itemOnSpawner);
        }
    }
}