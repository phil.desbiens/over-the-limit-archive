﻿using Harmony;
using UnityEngine;


namespace Play
{
    // Author : Zachary Derosby-Therrien
    [Findable(Tags.MainController)]
    public class AchievementGetEventChannel : MonoBehaviour
    {
        public event AchievementGetEvent OnAchievementGet;

        public void Publish(string achievementName)
        {
            OnAchievementGet?.Invoke(achievementName);
        }

        public delegate void AchievementGetEvent(string achievementName);
    }
}