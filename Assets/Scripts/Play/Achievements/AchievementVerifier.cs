﻿using Harmony;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public static class AchievementVerifier
    {
        public static bool AchievementCanBeUnlocked(AchievementsEnum achievement)
        {
            return Finder.Main.GameMode == GameModesEnum.SOLO && !SaveData.Current.achievements[(int) achievement];
        }
    }
}