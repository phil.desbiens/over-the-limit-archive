﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class AchievementNotifier : MonoBehaviour
    {
        [SerializeField] private string achievementGetNotificationFormat = "Achievement Unlocked :\n{0:0}";

        private AchievementGetEventChannel eventChannel;
        private AchievementNotificationView notificationView;

        private void Awake()
        {
            eventChannel = Finder.AchievementGetEventChannel;
            eventChannel.OnAchievementGet += NotifyAchievement;
            notificationView = GameObject.FindWithTag(Tags.AchievementNotifier)
                .GetComponent<AchievementNotificationView>();
        }

        private void OnDestroy()
        {
            eventChannel.OnAchievementGet -= NotifyAchievement;
        }

        private void NotifyAchievement(string achievementName)
        {
            IEnumerator Routine()
            {
                notificationView.UpdateText(String.Format(achievementGetNotificationFormat, achievementName));
                notificationView.enabled = true;

                yield return new WaitForSeconds(5);

                // Using a variable would add unnecessary code
                // ReSharper disable once Unity.InefficientPropertyAccess
                notificationView.enabled = false;
            }

            StartCoroutine(Routine());
        }
    }
}