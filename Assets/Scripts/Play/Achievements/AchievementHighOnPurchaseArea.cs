﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class AchievementHighOnPurchaseArea : MonoBehaviour
    {
        [SerializeField] private float timeLimit = 20f;

        private Main main;
        private AchievementGetEventChannel achievementGetEventChannel;

        public float timePassedInsideZone;
        private bool playerIsInsideZone;

        private void Awake()
        {
            main = Finder.Main;
            achievementGetEventChannel = Finder.AchievementGetEventChannel;

            if (main.GameMode == GameModesEnum.PVP
                || SaveData.Current.achievements[(int) AchievementsEnum.HIGH_ON_PURCHASE])
                Destroy(this);
        }

        private void Update()
        {
            if (playerIsInsideZone)
            {
                timePassedInsideZone += Time.deltaTime;

                if (timePassedInsideZone >= timeLimit)
                {
                    SaveData.Current.achievements[(int) AchievementsEnum.HIGH_ON_PURCHASE] = true;
                    achievementGetEventChannel.Publish(SaveData.HIGH_ON_PURCHASE_NAME);
                    Destroy(this);
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            PlayerController playerController = other.GetComponent<PlayerController>();
            if (playerController != null) playerIsInsideZone = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            PlayerController playerController = other.GetComponent<PlayerController>();
            if (playerController != null) playerIsInsideZone = false;
        }
    }
}