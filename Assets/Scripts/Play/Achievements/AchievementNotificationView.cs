﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class AchievementNotificationView : MonoBehaviour
    {
        private TMP_Text text;
        private Image image;

        private void Awake()
        {
            text = GetComponentInChildren<TMP_Text>();
            image = GetComponentInChildren<Image>();
            enabled = false;
        }

        private void OnDisable()
        {
            text.enabled = false;
            image.enabled = false;
        }

        private void OnEnable()
        {
            text.enabled = true;
            image.enabled = true;
        }

        public void UpdateText(string achievementName)
        {
            text.text = achievementName;
        }
    }
}