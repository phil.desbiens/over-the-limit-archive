﻿using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class ClerkEntity : MonoBehaviour
    {
        [SerializeField] private short amountLimit = 1000;

        private ClerkAnimator animator;
        private PlayerSensor playerSensor;

        public short AmountLimit => amountLimit;

        private void Awake()
        {
            playerSensor = GetComponent<PlayerSensor>();
            animator = GetComponent<ClerkAnimator>();

            playerSensor.OnPlayerSensed += StealObjects;
        }

        private void OnDestroy()
        {
            playerSensor.OnPlayerSensed -= StealObjects;
        }

        private void StealObjects(PlayerController playerController)
        {
            if (playerController.ShoppingCart.ShoppingCartValue > amountLimit)
            {
                playerController.ShoppingCart.LoseItems(amountLimit);
                animator.PlayOnStealAnimation();
            }
        }
    }
}