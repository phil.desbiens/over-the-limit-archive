﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class ClerkAnimator : BaseAnimator
    {
        public void PlayOnStealAnimation()
        {
            IEnumerator Routine()
            {
                Animator.SetBool(AnimatorParameters.IsStealing, true);
                yield return new WaitForSeconds(ANIMATION_TIME);
                Animator.SetBool(AnimatorParameters.IsStealing, false);
            }

            StartCoroutine(Routine());
        }
    }
}