﻿using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger & Guillaume Thivierge
    public class EnemyDespawner : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var disabler = other.gameObject.GetComponent<Disabler>();
            if (disabler != null) Destroy(other.gameObject);
        }
    }
}