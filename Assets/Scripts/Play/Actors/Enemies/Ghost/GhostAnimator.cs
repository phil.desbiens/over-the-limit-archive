﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class GhostAnimator : BaseAnimator
    {
        protected override void Awake()
        {
            Animator = GetComponentInChildren<Animator>();
            PreviousDirection = DirectionsEnum.RIGHT;
        }

        public void PlayOnStealAnimation()
        {
            IEnumerator Routine()
            {
                Animator.SetBool(AnimatorParameters.IsStealing, true);
                yield return new WaitForSeconds(ANIMATION_TIME);
                Animator.SetBool(AnimatorParameters.IsStealing, false);
            }

            StartCoroutine(Routine());
        }
    }
}