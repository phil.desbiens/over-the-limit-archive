﻿using System;
using Harmony;
using TMPro;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class GhostEntity : MonoBehaviour
    {
        [Header("Targets")] [Tooltip("Target A, to which the ghost roams")] [SerializeField]
        private string targetATag;

        [Tooltip("Target B, to which the ghost roams")] [SerializeField]
        private string targetBTag;

        [Tooltip("The main target that the ghost is chasing")] [SerializeField]
        private string mainTargetTag;

        [Header("Limits")] [Tooltip("The limit amount text format")] [SerializeField]
        private string amountFormat = "{0:0000} $";

        [Tooltip("The limit amount")] [SerializeField]
        private short amountLimit = 1000;

        private GameObject mainTarget;
        private GameObject targetA;
        private GameObject targetB;
        private GhostAnimator animator;
        private GhostMovement mover;
        private PlayerSensor playerSensor;
        private PlayerController player;
        private GameObject currentTarget;
        
        public short AmountLimit => amountLimit;
        
        private bool isRoamingToA;

        private void Awake()
        {
            targetA = GameObject.FindWithTag(targetATag);
            targetB = GameObject.FindWithTag(targetBTag);

            currentTarget = targetB;

            var limitText = GetComponentInChildren<TMP_Text>();
            animator = GetComponentInChildren<GhostAnimator>();
            mover = GetComponent<GhostMovement>();
            playerSensor = GetComponent<PlayerSensor>();

            limitText.text = amountFormat.Format(amountLimit);

            playerSensor.OnPlayerSensed += StealObjects;
        }

        private void Start()
        {
            mainTarget = GameObject.FindWithTag(mainTargetTag);
            player = mainTarget.GetComponent<PlayerController>();
        }

        private void OnDestroy()
        {
            playerSensor.OnPlayerSensed -= StealObjects;
        }

        private void Update()
        {
            ChooseTarget();
            MoveTowardsTarget();
        }

        private void MoveTowardsTarget()
        {
            var targetPosition = currentTarget.transform.position;
            var currentPosition = transform.position;

            var direction = currentPosition.DirectionTo(targetPosition);

            animator.FlipDirection((DirectionsEnum) Math.Sign(direction.x));
            mover.Move(currentPosition, targetPosition, direction.normalized);
        }

        private void ChooseTarget()
        {
            if (player.ShoppingCart.ShoppingCartValue > amountLimit)
                currentTarget = mainTarget;
            else currentTarget = isRoamingToA ? targetA : targetB;
        }

        public void SwapTarget()
        {
            isRoamingToA = !isRoamingToA;
        }

        private void StealObjects(PlayerController playerController)
        {
            if (playerController.ShoppingCart.ShoppingCartValue > amountLimit)
            {
                animator.PlayOnStealAnimation();
                playerController.ShoppingCart.LoseItems(amountLimit);
            }
        }
    }
}