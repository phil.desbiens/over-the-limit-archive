﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class GhostMovement : MonoBehaviour
    {
        [Header("Movement")] [Tooltip("The speed of the ghost")] [SerializeField] [Min(0)]
        private Vector2 speed = Vector2.one;

        [Header("Detection Range")] [Tooltip("The detection range of the ghost")] [SerializeField] [Min(0)]
        private float detectionRange = 0.1f;

        public void Move(Vector3 currentPosition, Vector3 targetPosition, Vector2 direction)
        {
            if (!currentPosition.IsCloseOf(targetPosition, detectionRange))
                transform.Translate(direction * speed * Time.deltaTime);
        }
    }
}