﻿using UnityEngine;

namespace Play
{
    // Author : Guillaume Thivierge
    public class EmployeeEntity : Disabler
    {
        [SerializeField] private float disableTime = 3f;

        private EmployeeAnimator animator;
        private new Rigidbody2D rigidbody2D;

        protected override float DisableTime => disableTime;

        private void Awake()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            animator = GetComponent<EmployeeAnimator>();
            Mover = transform.GetComponent<EmployeeMovement>();
        }

        private void Update()
        {
            if (!IsDisablingDisableable) Mover.Move();

            animator.UpdateAnimatorParameters(IsDisablingDisableable,
                rigidbody2D.velocity,
                (DirectionsEnum) Mover.MovementDirection.x);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var item = other.gameObject.GetComponent<ItemEntity>();
            if (item != null
                && item.Name != ItemNameEnum.DISCOUNT_TAG
                && item.Name != ItemNameEnum.DISCOUNT_TAG_LEVEL_THREE) DisableDisableable(item);
        }
    }
}