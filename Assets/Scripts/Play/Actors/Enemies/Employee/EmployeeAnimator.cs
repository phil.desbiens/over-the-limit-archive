﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class EmployeeAnimator : BaseAnimator
    {
        protected override void Awake()
        {
            base.Awake();
            PreviousDirection = DirectionsEnum.LEFT;
        }

        // Author : Zachary Desrosby-Therrien
        public void UpdateAnimatorParameters(bool isDisabling, Vector3 velocity, DirectionsEnum direction)
        {
            Animator.SetBool(AnimatorParameters.IsDisabling, isDisabling);
            Animator.SetBool(AnimatorParameters.IsFalling, Mathf.Abs(velocity.y) > 0);
            Animator.SetBool(AnimatorParameters.IsMoving, Mathf.Abs(velocity.x) > 0);

            FlipDirection(direction);
        }
    }
}