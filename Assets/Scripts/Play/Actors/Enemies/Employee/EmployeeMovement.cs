﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Guillaume Thivierge
    public class EmployeeMovement : AIMovement
    {
        [Header("Layer Masks")] [SerializeField]
        private LayerMask platformsLayerMask;

        [SerializeField] private LayerMask disableableLayerMask;

        private bool isFreeFalling;

        private new Rigidbody2D rigidbody2D;
        private new Collider2D collider2D;

        protected virtual LayerMask DisableableLayerMask => disableableLayerMask;
        protected virtual Collider2D AiObjectCollider2D => collider2D;
        protected override Rigidbody2D AiObjectRigidbody2D => rigidbody2D;

        private void Awake()
        {
            rigidbody2D = transform.GetComponent<Rigidbody2D>();
            collider2D = transform.GetComponent<Collider2D>();
            rigidbody2D.freezeRotation = true;
        }

        public override void Move()
        {
            if (IsTouchingGround())
            {
                if (isFreeFalling)
                {
                    isFreeFalling = false;
                    ChooseNewDirection();
                }

                AiObjectRigidbody2D.velocity = MovementDirection * speed;
            }
            else
            {
                isFreeFalling = true;
                AiObjectRigidbody2D.velocity = Vector2.down;
            }
        }

        private bool IsTouchingGround()
        {
            var bounds = collider2D.bounds;
            var raycastPlatform = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.down, objectDetectionRange, platformsLayerMask);

            return raycastPlatform.collider != null;
        }

        private void ChooseNewDirection()
        {
            var directionValue = RandomExtensions.GetOneOrMinusOneAtRandom();
            MovementDirection = directionValue > 0 ? Vector2.left : Vector2.right;
        }

        public override void StopMovingIfGrounded()
        {
            if (IsTouchingGround()) base.StopMovingIfGrounded();
        }
    }
}