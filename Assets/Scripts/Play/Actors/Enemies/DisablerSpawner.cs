﻿using System.Collections;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class DisablerSpawner : MonoBehaviour
    {
        [Tooltip("The Employee prefab to spawn")] [SerializeField]
        private GameObject prefabToSpawn;

        [Tooltip("The time between each Employee appearance")] [SerializeField]
        private float spawnDelay = 5f;

        private void OnEnable()
        {
            StartCoroutine(SpawnRoutine());
        }

        private IEnumerator SpawnRoutine()
        {
            while (isActiveAndEnabled)
            {
                yield return new WaitForSeconds(spawnDelay);
                Instantiate(prefabToSpawn, transform);
            }
        }
    }
}