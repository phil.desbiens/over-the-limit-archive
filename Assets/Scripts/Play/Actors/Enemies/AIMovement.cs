﻿using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public abstract class AIMovement : MonoBehaviour
    {
        [Tooltip("Distance from an object to sense")] [SerializeField] [Min(0)]
        protected float objectDetectionRange = 0.01f;

        [SerializeField] protected float speed = 3f;

        public Vector2 MovementDirection { get; set; }
        protected abstract Rigidbody2D AiObjectRigidbody2D { get; }

        public abstract void Move();

        public virtual void StopMovingIfGrounded()
        {
            AiObjectRigidbody2D.velocity = Vector2.zero;
        }
    }
}