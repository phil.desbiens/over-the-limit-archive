﻿using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class KarenMovement : AIMovement
    {
        [Header("Layer Masks")] [SerializeField]
        private LayerMask registerLayerMask;

        private new Rigidbody2D rigidbody2D;
        private new Collider2D collider2D;

        protected virtual LayerMask DisableableLayerMask => registerLayerMask;
        protected virtual Collider2D AiObjectCollider2D => collider2D;
        protected override Rigidbody2D AiObjectRigidbody2D => rigidbody2D;

        private void Awake()
        {
            rigidbody2D = transform.GetComponent<Rigidbody2D>();
            collider2D = transform.GetComponent<Collider2D>();
        }

        public override void Move()
        {
            AiObjectRigidbody2D.velocity = MovementDirection * speed;
        }
    }
}