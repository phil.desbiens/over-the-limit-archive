﻿using Harmony;

namespace Play
{
    // Author : Philippe Desbiens
    public class KarenAnimator : BaseAnimator
    {
        protected override void Awake()
        {
            base.Awake();
            PreviousDirection = DirectionsEnum.RIGHT;
        }

        // Author : Zachary Derosby-Therrien
        public void UpdateAnimatorParameters(bool isDisabling, DirectionsEnum direction)
        {
            Animator.SetBool(AnimatorParameters.IsDisabling, isDisabling);
            Animator.SetBool(AnimatorParameters.IsMoving, !isDisabling);

            FlipDirection(direction);
        }
    }
}