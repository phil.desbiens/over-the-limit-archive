﻿using Harmony;
using UnityEngine;
using Random = System.Random;

namespace Play
{
    // Author : Kaylen Bélanger
    public class KarenEntity : Disabler
    {
        [Tooltip("The time the register will be disabled when the Karen touches it")] [SerializeField] [Min(0)]
        private float disableTime = 3f;

        private static readonly Random Rnd = new Random();

        private readonly string disableableTag = Tags.Register;

        private GameObject[] gameObjectsDisableables;
        private KarenAnimator animator;

        protected override float DisableTime => disableTime;

        private void Awake()
        {
            animator = GetComponent<KarenAnimator>();
            Mover = transform.GetComponent<KarenMovement>();

            FindDisableables();
            PickRandomDisableableToDisable();
        }

        private void Update()
        {
            if (!IsDisablingDisableable) Mover.Move();

            animator.UpdateAnimatorParameters(IsDisablingDisableable, (DirectionsEnum) Mover.MovementDirection.x);
        }

        private void OnEnable()
        {
            ChooseMovementDirection();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var cashRegister = other.gameObject.GetComponent<CashRegister>();
            if (cashRegister != null)
            {
                DisableDisableable(cashRegister);
            }
        }

        private void PickRandomDisableableToDisable()
        {
            if (gameObjectsDisableables != null)
                DisabledDisableableGameObject = gameObjectsDisableables[Rnd.Next(gameObjectsDisableables.Length)];
        }

        private void FindDisableables()
        {
            gameObjectsDisableables = GameObject.FindGameObjectsWithTag(disableableTag);
        }

        private void ChooseMovementDirection()
        {
            if (DisabledDisableableGameObject != null)
            {
                if (transform.position.x < DisabledDisableableGameObject.transform.position.x)
                    Mover.MovementDirection = Vector2.right;
                else if (transform.position.x > DisabledDisableableGameObject.transform.position.x)
                    Mover.MovementDirection = Vector2.left;
            }
        }
    }
}