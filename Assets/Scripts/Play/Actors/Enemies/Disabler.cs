﻿using System.Collections;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger & Guillaume Thivierge
    public abstract class Disabler : MonoBehaviour
    {
        protected GameObject DisabledDisableableGameObject;
        protected AIMovement Mover;

        protected abstract float DisableTime { get; }
        protected bool IsDisablingDisableable { get; private set; }
        
        protected void DisableDisableable(IDisableable disableable)
        {
            IEnumerator Routine()
            {
                disableable.Disable();
                Mover.StopMovingIfGrounded();
                IsDisablingDisableable = true;

                yield return new WaitForSeconds(DisableTime);

                // Author : Guillaume Thivierge
                IsDisablingDisableable = false;
                disableable.Enable();
            }

            StartCoroutine(Routine());
        }
    }
}