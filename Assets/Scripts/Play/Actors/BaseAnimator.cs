﻿using UnityEngine;

namespace Play
{
    public abstract class BaseAnimator : MonoBehaviour
    {
        protected Animator Animator;
        protected DirectionsEnum PreviousDirection;

        protected const float UPWARD_VELOCITY_THRESHOLD = 0.01f;
        protected const float DOWNWARD_VELOCITY_THRESHOLD = -0.01f;
        protected const int ANIMATION_TIME = 1;

        protected virtual void Awake()
        {
            Animator = GetComponent<Animator>();
        }

        public void FlipDirection(DirectionsEnum direction)
        {
            if (direction == DirectionsEnum.NONE || direction == PreviousDirection) return;

            var localScale = Animator.transform.localScale;

            // We want to modify the position of the item, using a variable would prevent that or add unnecessary code
            // ReSharper disable once Unity.InefficientPropertyAccess
            Animator.transform.localScale = new Vector3(localScale.x * -1, localScale.y, localScale.z);
            PreviousDirection = direction;
        }
    }
}