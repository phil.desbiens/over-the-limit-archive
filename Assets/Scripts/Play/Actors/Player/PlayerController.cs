using Harmony;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Play
{
    // Author : Gabriel Fortier
    public class PlayerController : MonoBehaviour
    {
        private Game game;
        private PlayerMover mover;
        private PlayerAnimator animator;
        private PlayerInteractor interactor;
        private PlatformSensor platformSensor;

        private Vector2 movement;

        public ShoppingCart ShoppingCart { get; private set; }

        // Author : Guillaume Thivierge
        public delegate void ItemPickedUpEvent(ItemEntity sender);

        public event ItemPickedUpEvent OnItemPickedUp;

        private void Awake()
        {
            game = Finder.Game;

            // Author : Kaylen Bélanger
            mover = GetComponent<PlayerMover>();

            // Philippe Desbiens
            interactor = GetComponent<PlayerInteractor>();
            animator = GetComponent<PlayerAnimator>();

            ShoppingCart = GetComponent<ShoppingCart>();
            platformSensor = GetComponent<PlatformSensor>();

            platformSensor.OnPlatformSensed += GoThrough;
        }

        private void OnDestroy()
        {
            platformSensor.OnPlatformSensed -= GoThrough;
        }

        private void Update()
        {
            mover.Move(movement);

            // Philippe Desbiens
            animator.UpdateAnimatorParameters(movement, mover.Rigidbody2D.velocity.y);
        }

        [UsedImplicitly]
        public void OnPause(InputAction.CallbackContext context)
        {
            if (context.performed) // Perform action phase
                game.TogglePauseMenu();
        }

        [UsedImplicitly]
        public void OnInteract(InputAction.CallbackContext context)
        {
            if (context.performed) // Perform action phase
                interactor.Interact(this);
        }

        [UsedImplicitly]
        public void OnMovement(InputAction.CallbackContext context)
        {
            movement = context.ReadValue<Vector2>();
        }

        [UsedImplicitly]
        public void OnJump(InputAction.CallbackContext context)
        {
            if (context.performed) // Perform action phase
                mover.Jump();
        }

        [UsedImplicitly]
        public void OnClimb(InputAction.CallbackContext context)
        {
            var direction = context.ReadValue<Vector2>();

            if (direction.y != 0) mover.Climb(direction);
        }

        // Author : Guillaume Thivierge
        public void StartGrabbingProcess(ItemEntity item)
        {
            ShoppingCart.AddItem(item);
            NotifyItemGrabbed(item);
        }

        // Author : Guillaume Thivierge
        private void NotifyItemGrabbed(ItemEntity item)
        {
            OnItemPickedUp?.Invoke(item);
        }

        // Author : Guillaume Thivierge
        private void GoThrough(Platform platform)
        {
            if (movement.y <= mover.DownwardDirection) mover.GoThrough(platform);
        }
    }
}