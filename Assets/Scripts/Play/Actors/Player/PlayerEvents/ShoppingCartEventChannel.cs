﻿using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class ShoppingCartEventChannel : MonoBehaviour
    {
        public event ShoppingCartEvent OnShoppingCartValueUpdate;

        public void Publish()
        {
            OnShoppingCartValueUpdate?.Invoke();
        }
    }

    public delegate void ShoppingCartEvent();
}