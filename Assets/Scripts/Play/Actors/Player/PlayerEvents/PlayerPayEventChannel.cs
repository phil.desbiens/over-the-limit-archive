﻿using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class PlayerPayEventChannel : MonoBehaviour
    {
        public event PlayerPayEvent OnPlayerPay;

        public void Publish()
        {
            OnPlayerPay?.Invoke();
        }
    }

    public delegate void PlayerPayEvent();
}