using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Play
{
   // Author : Guillaume Thivierge
    public class ShoppingCart : MonoBehaviour
    {
        [Tooltip("the multiplier when its dark the level3 and the player sell his items")]
        [SerializeField] private float level3DarkTimeMultiplier = 1.2f;

        [SerializeField] private int requiredKatanas = 6;
        
        private const int BONUS_AMOUNT_COMPLETE_SET = 750;
        
        private readonly Stack<ItemEntity> itemsInShoppingCart = new Stack<ItemEntity>();
        private readonly bool[] itemsPartOfSetBought = {false, false, false, false, false};
        
        private Main main;
        private Game game;
        
        private PlayerPayEventChannel playerPayEventChannel;
        private ShoppingCartEventChannel shoppingCartEventChannel;
        private AchievementGetEventChannel achievementGetEventChannel;
        
        private Level3LightManager lightInLevel3;

        private int nbKatanaSold;
        
        public bool HasBoughtBaguetteOnly { get; private set; } = true;
        public int MoneySpent { get; private set; }
        public int ShoppingCartValue => itemsInShoppingCart.Sum(item => item.CurrentValue);
        
        // Author : Gabriel Fortier
        private void Awake()
        {
            game = Finder.Game;
            main = Finder.Main;
            achievementGetEventChannel = Finder.AchievementGetEventChannel;

            playerPayEventChannel = GetComponent<PlayerPayEventChannel>();
            shoppingCartEventChannel = GetComponent<ShoppingCartEventChannel>();
        }

        private void Start()
        {
            if (game.CurrentLevel == LevelsEnum.LEVEL_THREE)
                lightInLevel3 = GameObject.FindWithTag(Tags.Light).GetComponent<Level3LightManager>();
        }

        // Author : Zachary Derosby-Therrien
        // BR : Ben Approved
        public void AddItem(ItemEntity obj)
        {
            if (obj.Name == ItemNameEnum.DISCOUNT_TAG
            || obj.Name == ItemNameEnum.DISCOUNT_TAG_LEVEL_THREE)
            {
                foreach (var item in itemsInShoppingCart) item.ApplyDiscount();
            }
            else
            {
                // Author : Kaylen Bélanger
                main.PickUpSound.PlayOneShot(main.PickUpSound.clip);
                itemsInShoppingCart.Push(obj);
            }

            shoppingCartEventChannel.Publish();
        }

        // Author : Philippe Desbiens
        public void BuyItem()
        {
            if (itemsInShoppingCart.Count > 0)
            {
                var item = itemsInShoppingCart.Pop();

                if (game.CurrentLevel == LevelsEnum.LEVEL_THREE && lightInLevel3.LevelIsInDark())
                {
                    item.CurrentValue = (int) (item.CurrentValue * level3DarkTimeMultiplier);
                    
                    if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.POTION_SELLER))
                        HandlePotionSellerAchievement(item);
                }

                MoneySpent += item.CurrentValue;

                // Author : Zachary Derosby-Therrien
                if (game.CurrentLevel == LevelsEnum.LEVEL_TWO)
                {
                    if (IsBonusGranted(item))
                    {
                        MoneySpent += BONUS_AMOUNT_COMPLETE_SET;
                        for (int i = 0; i < 5; i++)
                        {
                            itemsPartOfSetBought[i] = false;
                        }
                    }
                        
                }

                if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.BAGUETTE)
                    && HasBoughtBaguetteOnly
                    && item.Name != ItemNameEnum.BREAD)
                {
                    HasBoughtBaguetteOnly = false;
                }

                if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.WEEB_ARSENAL))
                    HandleWeebArsenalAchievement(item);
                

                if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.GET_IT_WHILE_ITS_HOT))
                    HandleGetItWhileHotAchievement(item);

                // Author : Gabriel Fortier
                playerPayEventChannel.Publish();
                shoppingCartEventChannel.Publish();
                
                // Author : Guillaume Thivierge
                main.BuyingSound.PlayOneShot(main.BuyingSound.clip);
            }
        }

        // Author : Gabriel Fortier
        public void LoseItems(int limit)
        {
            if (AchievementVerifier.AchievementCanBeUnlocked(AchievementsEnum.RED_CARD))
                HandleRedCardAchievement(limit);

            while (ShoppingCartValue > limit) itemsInShoppingCart.Pop();
            shoppingCartEventChannel.Publish();
        }

        // Author : Zachary Derosby-Therrien
        private bool IsBonusGranted(ItemEntity item)
        {
            itemsPartOfSetBought[(int) item.Name % 5] = true;

            return itemsPartOfSetBought.All(itemIsBought => itemIsBought);
        }

        // Author : Zachary Derosby-Therrien
        private void HandleGetItWhileHotAchievement(ItemEntity item)
        {
            if (item.Name == ItemNameEnum.BROWNIE
                && game.DefaultTime - game.TimeRemaining <= 20)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.GET_IT_WHILE_ITS_HOT] = true;
                achievementGetEventChannel.Publish(SaveData.WHILE_HOT_ACHIEVEMENT_NAME);
            }
        }

        // Author : Zachary Derosby-Therrien
        private void HandleWeebArsenalAchievement(ItemEntity item)
        {
            if (item.Name == ItemNameEnum.SWORD)
            {
                nbKatanaSold++;
                if (nbKatanaSold >= requiredKatanas)
                {
                    SaveData.Current.achievements[(int) AchievementsEnum.WEEB_ARSENAL] = true;
                    achievementGetEventChannel.Publish(SaveData.WEEB_ARSENAL_ACHIEVEMENT_NAME);
                }
            }
        }

        // Author : Zachary Derosby-Therrien
        private void HandlePotionSellerAchievement(ItemEntity item)
        {
            if (item.Name == ItemNameEnum.WHITE_POTION)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.POTION_SELLER] = true;
                achievementGetEventChannel.Publish(SaveData.POTION_SELLER_ACHIEVEMENT_NAME);
            }
        }

        // Author : Zachary Derosby-Therrien
        private void HandleRedCardAchievement(int limit)
        {
            if (ShoppingCartValue >= limit * 2)
            {
                SaveData.Current.achievements[(int) AchievementsEnum.RED_CARD] = true;
                achievementGetEventChannel.Publish(SaveData.RED_CARD_ACHIEVEMENT_NAME);
            }
        }
    }

}