﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class PlayerMover : MonoBehaviour
    {
        [Header("Movement")] [Tooltip("The speed of the character")] [SerializeField] [Min(0)]
        private float baseSpeed = 8f;

        [Tooltip("The climbing speed of the character")] [SerializeField] [Min(0)]
        private float climbSpeed = 5f;

        [Tooltip("The force of the player's jump")] [SerializeField] [Min(0)]
        private float jumpForce = 8f;

        [Tooltip("The limit of the vertical velocity")] [SerializeField] [Min(0)]
        private float verticalVelocityLimit = 8f;

        [Tooltip("Distance from an obstacle to sense")] [SerializeField] [Min(0)]
        private float obstacleDetectionRange = 0.02f;

        [Tooltip("Distance from the floor to sense")] [SerializeField] [Min(0)]
        private float floorDetectionRange = 0.2f;

        [Tooltip("Distance from a ladder to sense")] [SerializeField] [Min(0)]
        private float ladderDetectionRange = 0.01f;

        [SerializeField] private float passThroughDelay = 0.4f;
        
        [SerializeField] [Range(0f, -1f)] private float downwardDirection = -0.5f;

        [Header("Layer Masks")] [SerializeField]
        private LayerMask platformsLayerMask;

        [SerializeField] private LayerMask obstacleLayerMask;
        [SerializeField] private LayerMask invisibleWallLayerMask;
        [SerializeField] private LayerMask ladderLayerMask;

        // Author : Guillaume Thivierge
        private AudioSource runningSound;

        // Author : Philippe Desbiens
        private AudioSource jumpSound;

        private new Collider2D collider2D;
        public float DownwardDirection => downwardDirection;
        private float gravityScale;

        public Rigidbody2D Rigidbody2D { get; private set; }

        public float Speed
        {
            get => baseSpeed;
            set => baseSpeed = value;
        }

        private void Awake()
        {
            runningSound = Finder.Main.RunningSound;
            jumpSound = Finder.Main.JumpSound;

            Rigidbody2D = transform.GetComponent<Rigidbody2D>();
            collider2D = transform.GetComponent<Collider2D>();

            Rigidbody2D.freezeRotation = true;
            gravityScale = Rigidbody2D.gravityScale;
        }

        private void OnDestroy()
        {
            if (runningSound != null)
                runningSound.Stop();

            if (jumpSound != null)
                jumpSound.Stop();
        }

        private bool CanMoveTo(float direction)
        {
            return direction < 0 && !IsTouchingLeftWall()
                   || direction > 0 && !IsTouchingRightWall();
        }

        public void Move(Vector2 direction)
        {
            if (CanMoveTo(direction.x))
                transform.Translate(new Vector2(direction.x, 0) * (baseSpeed * Time.deltaTime));

            // Author : Guillaume Thivierge
            if (runningSound.isPlaying)
            {
                if (direction.x == 0 || !IsTouchingGround()) runningSound.Stop();
            }
            else if (direction.x != 0 && IsTouchingGround()) runningSound.Play();
        }

        // Author : Philippe Desbiens & Kaylen Bélanger
        public void Jump()
        {
            if (IsTouchingGround())
            {
                var verticalVelocity = Rigidbody2D.velocity.y;

                if (verticalVelocity + jumpForce <= verticalVelocityLimit)
                    Rigidbody2D.velocity += Vector2.up * jumpForce;

                else Rigidbody2D.velocity = new Vector2(0, verticalVelocityLimit);

                runningSound.Stop();
                jumpSound.Play();
            }
        }

        public void Climb(Vector2 direction)
        {
            if (IsTouchingLadder() && direction.y > DownwardDirection)
            {
                var newVerticalVelocity = direction.y * climbSpeed;

                Rigidbody2D.velocity = new Vector2(Rigidbody2D.velocity.x, newVerticalVelocity);
                Rigidbody2D.gravityScale = 0;
            }
            else Rigidbody2D.gravityScale = gravityScale;
        }

        public void GoThrough(Platform platform)
        {
            var boxCollider2D = GetComponent<BoxCollider2D>();

            IEnumerator Fall()
            {
                Physics2D.IgnoreCollision(platform.BoxCollider2D, boxCollider2D, true);
                yield return new WaitForSeconds(passThroughDelay);
                Physics2D.IgnoreCollision(platform.BoxCollider2D, boxCollider2D, false);
            }

            StartCoroutine(Fall());
        }

        private bool IsTouchingGround()
        {
            var bounds = collider2D.bounds;

            var raycastPlatform = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.down, floorDetectionRange, platformsLayerMask);

            var raycastObstacle = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.down, floorDetectionRange, obstacleLayerMask);

            return raycastPlatform.collider != null || raycastObstacle.collider != null;
        }

        private bool IsTouchingLeftWall()
        {
            var bounds = collider2D.bounds;

            var raycastLeftWall = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.left, obstacleDetectionRange, obstacleLayerMask);

            var raycastInvisibleLeftWall = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.left, obstacleDetectionRange, invisibleWallLayerMask);

            return raycastLeftWall.collider != null || raycastInvisibleLeftWall.collider != null;
        }

        private bool IsTouchingRightWall()
        {
            var bounds = collider2D.bounds;

            var raycastRightWall = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.right, obstacleDetectionRange, obstacleLayerMask);

            var raycastInvisibleRightWall = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.right, obstacleDetectionRange, invisibleWallLayerMask);

            return raycastRightWall.collider != null || raycastInvisibleRightWall.collider != null;
        }

        private bool IsTouchingLadder()
        {
            var bounds = collider2D.bounds;

            var raycastRightWall = Physics2D.BoxCast(bounds.center, bounds.size, 0f,
                Vector2.up, ladderDetectionRange, ladderLayerMask);

            return raycastRightWall.collider != null;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var ladder = GameObject.FindWithTag(Tags.Ladder);
            if (ladder != null) Rigidbody2D.gravityScale = gravityScale;

            var item = other.gameObject.GetComponent<ItemPriceTag>();
            if (item != null) item.ChangeTextToDefault();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var item = other.gameObject.GetComponent<ItemPriceTag>();
            if (item != null) item.ChangeTextToGreen();
        }
    }
}