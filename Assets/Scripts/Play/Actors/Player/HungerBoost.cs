﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;
using System.Collections;

namespace Play
{
    // Author : Guillaume Thivierge
    public class HungerBoost : MonoBehaviour
    {
        [SerializeField] private List<ItemNameEnum> itemEnums = new List<ItemNameEnum>();
        [SerializeField] [Min(1)] private float hungerTime = 15f;
        [SerializeField] [Min(1)] private float hungerDownTime = 15f;
        [SerializeField] [Min(1)] private float speedBoostBonus = 1.25f;

        private ItemsFactory factory;
        private PlayerMover playerMover;
        private PlayerController player;
        private SpriteRenderer hungerItemImage;
        
        private bool playerIsBoosted;

        private ItemEntity HungerItem { get; set; }

        private void Awake()
        {
            factory = Finder.ItemsFactory;
            
            playerMover = GetComponentInParent<PlayerMover>();
            player = GetComponentInParent<PlayerController>();
            
            hungerItemImage = GetComponent<SpriteRenderer>();
        }

        private void OnPlayerItemPickedUp(ItemEntity item)
        {
            if (HungerItem == null || HungerItem.Name != item.Name) return;

            IEnumerator PlayerBoostRoutine()
            {
                var normalSpeed = playerMover.Speed;
                playerMover.Speed *= speedBoostBonus;
                playerIsBoosted = true;

                yield return new WaitForSeconds(hungerTime);

                playerMover.Speed = normalSpeed;
                playerIsBoosted = false;
            }

            if (!playerIsBoosted) StartCoroutine(PlayerBoostRoutine());
        }

        private void OnEnable()
        {
            player.OnItemPickedUp += OnPlayerItemPickedUp;
            StartCoroutine(HungryRoutine());
        }

        private void OnDisable()
        {
            player.OnItemPickedUp -= OnPlayerItemPickedUp;
        }

        private IEnumerator HungryRoutine()
        {
            while (isActiveAndEnabled)
            {
                HungerItem = factory.GetItemToSpawn(itemEnums.Random());
                
                hungerItemImage.sprite = HungerItem.GetComponent<SpriteRenderer>().sprite;
                hungerItemImage.enabled = true;

                yield return new WaitForSeconds(hungerTime);

                HungerItem = null;

                // Complicates code readability
                // ReSharper disable once Unity.InefficientPropertyAccess
                hungerItemImage.enabled = false;

                yield return new WaitForSeconds(hungerDownTime);
            }
        }
    }
}