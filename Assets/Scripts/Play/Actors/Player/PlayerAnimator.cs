﻿using System;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class PlayerAnimator : BaseAnimator
    {
        protected override void Awake()
        {
            base.Awake();
            PreviousDirection = DirectionsEnum.RIGHT;
        }

        public void UpdateAnimatorParameters(Vector2 movement, float verticalVelocity)
        {
            Animator.SetFloat(AnimatorParameters.HorizontalVelocity, Mathf.Abs(movement.x));

            Animator.SetBool(AnimatorParameters.IsJumping, verticalVelocity >= UPWARD_VELOCITY_THRESHOLD);
            Animator.SetBool(AnimatorParameters.IsFalling, verticalVelocity <= DOWNWARD_VELOCITY_THRESHOLD);

            FlipDirection((DirectionsEnum) Math.Sign(movement.x));
        }
    }
}