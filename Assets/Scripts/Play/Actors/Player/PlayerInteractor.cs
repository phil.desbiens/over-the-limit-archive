﻿using UnityEngine;

namespace Play
{
    // Author : Philippe Desbiens
    public class PlayerInteractor : MonoBehaviour
    {
        public IInteractable NearestInteractable { get; set; }

        public void Interact(PlayerController playerEntity)
        {
            NearestInteractable?.Interact(playerEntity);
        }
    }
}