﻿namespace Play
{
    // Author : Guillaume Thivierge
    public interface IDisableable
    {
        void Disable();
        void Enable();
    }
}