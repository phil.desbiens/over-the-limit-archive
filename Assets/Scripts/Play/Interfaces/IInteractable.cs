﻿namespace Play
{
    // Author : Zachary Derosby-Therrien
    public interface IInteractable
    {
        void Interact(PlayerController sender);
    }
}