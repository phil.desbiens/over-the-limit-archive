﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class LevelEndStatsScreen : MonoBehaviour
    {
        [SerializeField] private string levelTitleFormat = "Level {0:0}";
        [SerializeField] private string levelStatsScoreFormat = "Score :\t\t\t{0:0 0} $";
        [SerializeField] private string levelStatsGoalFormat = "Objective :\t\t\t{0:0 0} $";
        [SerializeField] private string levelIsCompleteText = "Next Level";
        [SerializeField] private string levelIsIncompleteText = "Try Again";

        private const string LEVEL_COMPLETE_STATE = "Complete";
        private const string LEVEL_INCOMPLETE_STATE = "Failed";

        private Button continueButton;
        private Game game;

        private void Awake()
        {
            game = Finder.Game;

            Button[] buttons = GetComponentsInChildren<Button>();
            continueButton = buttons.WithName(GameObjects.Continuer);

            UpdateView();

            Finder.Main.EndLevelSound.Play();
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            continueButton.Select();
        }

        private void OnEnable()
        {
            continueButton.onClick.AddListener(OnContinueButtonClick);
        }

        private void OnDisable()
        {
            continueButton.onClick.RemoveListener(OnContinueButtonClick);
        }

        private void UpdateView()
        {
            string levelState;
            if (game.IsLevelWon)
            {
                continueButton.GetComponentInChildren<TMP_Text>().text = levelIsCompleteText;
                levelState = LEVEL_COMPLETE_STATE;
            }
            else
            {
                continueButton.GetComponentInChildren<TMP_Text>().text = levelIsIncompleteText;
                levelState = LEVEL_INCOMPLETE_STATE;
            }

            var titleText = transform.Find(GameObjects.LevelTitle).GetComponent<TMP_Text>();
            titleText.text = string.Format(levelTitleFormat, levelState);

            var statsTexts = transform.Find(GameObjects.LevelStats).GetComponentsInChildren<TMP_Text>();
            statsTexts[0].text = string.Format(levelStatsScoreFormat, game.HighestMoneySpent);
            statsTexts[1].text = string.Format(levelStatsGoalFormat, game.SpendingGoal);
        }

        private void OnContinueButtonClick()
        {
            game.EndLevel();
        }
    }
}