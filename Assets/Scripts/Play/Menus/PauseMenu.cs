﻿using System.Collections;
using Game;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Gabriel Fortier
    public class PauseMenu : MonoBehaviour
    {
        private Game game;
        private Main main;
        private InputActions.MenuActions menuInputs;
        private Button optionsButton;
        private Button quitButton;
        private Button resumeButton;

        // Author : Guillaume Thivierge
        private Button achievementsButton;

        private void Awake()
        {
            main = Finder.Main;
            game = Finder.Game;

            Button[] buttons = GetComponentsInChildren<Button>();
            resumeButton = buttons.WithName(GameObjects.PauseResume);
            quitButton = buttons.WithName(GameObjects.PauseQuit);
            optionsButton = buttons.WithName(GameObjects.PauseOptions);

            // Author : Guillaume Thivierge
            achievementsButton = buttons.WithName(GameObjects.AchievementsButton);
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            resumeButton.Select();
        }

        private void OnEnable()
        {
            resumeButton.onClick.AddListener(ResumeGame);
            quitButton.onClick.AddListener(QuitGame);
            optionsButton.onClick.AddListener(OptionsGame);

            // Author : Guillaume Thivierge
            achievementsButton.onClick.AddListener(OpenAchievement);
        }

        private void OnDisable()
        {
            resumeButton.onClick.RemoveListener(ResumeGame);
            quitButton.onClick.RemoveListener(QuitGame);
            optionsButton.onClick.RemoveListener(OptionsGame);

            // Author : Guillaume Thivierge
            achievementsButton.onClick.RemoveListener(OpenAchievement);
        }

        private void ResumeGame()
        {
            IEnumerator Routine()
            {
                TimeManager.ResumeGame();
                yield return main.UnloadCurrentScene();
            }

            StartCoroutine(Routine());
        }

        private void QuitGame()
        {
            IEnumerator Routine()
            {
                TimeManager.ResumeGame();

                main.MenuMusic.Play();

                yield return game.UnloadCurrentLevel();
                yield return game.UnloadUI();
                yield return main.ResetToMenuScene();
            }

            StartCoroutine(Routine());
        }

        private void OptionsGame()
        {
            IEnumerator Routine()
            {
                yield return main.LoadOptionsScenes();
            }

            StartCoroutine(Routine());
        }

        // Author : Guillaume Thivierge
        private void OpenAchievement()
        {
            IEnumerator Routine()
            {
                yield return main.LoadAchievementScenes();
            }

            StartCoroutine(Routine());
        }
    }
}