﻿using System;
using System.Collections;
using System.Linq;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Guillaume Thivierge
    public class AchievementsMenu : MonoBehaviour
    {
        private Main main;
        private Button returnButton;

        private readonly TMP_Text[] achievements = new TMP_Text[Enum.GetNames(typeof(AchievementsEnum)).Length];
        private GameObject easterEgg;
        
        
        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            returnButton = buttons.WithName(GameObjects.Return);
            
            TMP_Text[] achievementsGameObjects = GetComponentsInChildren<TMP_Text>();
            achievements[(int) AchievementsEnum.WEEB_ARSENAL] =
                achievementsGameObjects.WithName(GameObjects.WeebArsenal);

            achievements[(int) AchievementsEnum.HIGH_ON_PURCHASE] =
                achievementsGameObjects.WithName(GameObjects.HighOnPurchase);

            achievements[(int) AchievementsEnum.CAPITAL_TWO] =
                achievementsGameObjects.WithName(GameObjects.Capital2);

            achievements[(int) AchievementsEnum.RED_CARD] =
                achievementsGameObjects.WithName(GameObjects.RedCard);

            achievements[(int) AchievementsEnum.SAFE_RIDE] =
                achievementsGameObjects.WithName(GameObjects.SafeRide);

            achievements[(int) AchievementsEnum.GET_IT_WHILE_ITS_HOT] =
                achievementsGameObjects.WithName(GameObjects.GetItWhileItsHot);

            achievements[(int) AchievementsEnum.BAGUETTE] =
                achievementsGameObjects.WithName(GameObjects.WillYouFinish);

            achievements[(int) AchievementsEnum.POTION_SELLER] =
                achievementsGameObjects.WithName(GameObjects.PotionSeller);
            
            easterEgg = GameObject.Find(GameObjects.AchievementEasterEgg).gameObject;
        }

        private void ToggleEasterEgg()
        {
            bool hasUnlockedAllAchievements = SaveData.Current.achievements.All(achievement => achievement);
            easterEgg.SetActive(hasUnlockedAllAchievements);
        }

        private void Start()
        {
            returnButton.Select();
        }

        private void OnEnable()
        {
            returnButton.onClick.AddListener(OnQuitButtonClick);
            HideLockedAchievements();
            ToggleEasterEgg();
        }

        private void HideLockedAchievements()
        {
            var currentAchievements = SaveData.Current.achievements;
            for (var i = 0; i < Enum.GetNames(typeof(AchievementsEnum)).Length; i++)
            {
                if (!currentAchievements[i]) achievements[i].alpha = 0.4f;
            }
        }

        private void OnDisable()
        {
            returnButton.onClick.RemoveListener(OnQuitButtonClick);
        }

        private void OnQuitButtonClick()
        {
            IEnumerator Routine()
            {
                yield return main.ReturnPreviousScene();
            }

            StartCoroutine(Routine());
        }
    }
}