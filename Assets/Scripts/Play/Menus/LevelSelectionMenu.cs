﻿using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Guillaume Thivierge
    public class LevelSelectionMenu : MonoBehaviour
    {
        private Main main;
        private Game game;

        private Button levelTutorialButton;
        private Button levelOneButton;
        private Button levelTwoButton;
        private Button levelThreeButton;
        private Button levelFourButton;
        
        private Button soloButton;
        private Button pvpButton;
        
        private Button achievementButton;
        
        private Button returnButton;

        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            levelTutorialButton = buttons.WithName(GameObjects.LevelTutorial);
            levelOneButton = buttons.WithName(GameObjects.LevelOne);
            levelTwoButton = buttons.WithName(GameObjects.LevelTwo);
            levelThreeButton = buttons.WithName(GameObjects.LevelThree);
            levelFourButton = buttons.WithName(GameObjects.LevelFour);
           
            soloButton = buttons.WithName(GameObjects.Solo);
            pvpButton = buttons.WithName(GameObjects.Pvp);

            achievementButton = buttons.WithName(GameObjects.Achievements);

            returnButton = buttons.WithName(GameObjects.Return);

            SaveVerification();
        }

        private void Start()
        {
            if (main.GameMode == GameModesEnum.SOLO) soloButton.interactable = false;
            else pvpButton.interactable = false;

            // Author : Philippe Desbiens
            levelTutorialButton.Select();
        }

        private void OnEnable()
        {
            levelTutorialButton.onClick.AddListener(LoadLevelTutorial);
            levelOneButton.onClick.AddListener(LoadLevelOne);
            levelTwoButton.onClick.AddListener(LoadLevelTwo);
            levelThreeButton.onClick.AddListener(LoadLevelThree);
            levelFourButton.onClick.AddListener(LoadLevelFour);

            soloButton.onClick.AddListener(SelectSoloMode);
            pvpButton.onClick.AddListener(SelectPvpMode);
            
            achievementButton.onClick.AddListener(OpenAchievement);
            
            returnButton.onClick.AddListener(ReturnSaveScreen);

            ToggleTutorial(main.GameMode != GameModesEnum.PVP);
        }

        private void OnDisable()
        {
            levelTutorialButton.onClick.RemoveListener(LoadLevelTutorial);
            levelOneButton.onClick.RemoveListener(LoadLevelOne);
            levelTwoButton.onClick.RemoveListener(LoadLevelTwo);
            levelThreeButton.onClick.RemoveListener(LoadLevelThree);
            levelFourButton.onClick.RemoveListener(LoadLevelFour);

            soloButton.onClick.RemoveListener(SelectSoloMode);
            pvpButton.onClick.RemoveListener(SelectPvpMode);
            
            achievementButton.onClick.RemoveListener(OpenAchievement);
            
            returnButton.onClick.RemoveListener(ReturnSaveScreen);
        }

        private void LoadLevelTutorial()
        {
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes(LevelsEnum.LEVEL_TUTORIAL);
            }

            StartCoroutine(Routine());
        }

        private void LoadLevelOne()
        {
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes(LevelsEnum.LEVEL_ONE);
            }

            StartCoroutine(Routine());
        }

        private void LoadLevelTwo()
        {
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes(LevelsEnum.LEVEL_TWO);
            }

            StartCoroutine(Routine());
        }

        private void LoadLevelThree()
        {
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes(LevelsEnum.LEVEL_THREE);
            }

            StartCoroutine(Routine());
        }

        private void LoadLevelFour()
        {
            IEnumerator Routine()
            {
                yield return main.LoadGameScenes(LevelsEnum.LEVEL_FOUR);
            }

            StartCoroutine(Routine());
        }

        private void SelectSoloMode()
        {
            main.GameMode = GameModesEnum.SOLO;
            soloButton.interactable = false;
            pvpButton.interactable = true;

            pvpButton.Select();

            ToggleTutorial(true);

            main.MenuButtonSound.Play();
        }

        private void SelectPvpMode()
        {
            main.GameMode = GameModesEnum.PVP;
            pvpButton.interactable = false;
            soloButton.interactable = true;

            soloButton.Select();

            ToggleTutorial(false);

            main.MenuButtonSound.Play();
        }
        
        private void OpenAchievement()
        {
            IEnumerator Routine()
            {
                yield return main.LoadAchievementScenes();
            }

            StartCoroutine(Routine());
        }
        
        private void ReturnSaveScreen()
        {
            IEnumerator Routine()
            {
                yield return main.ReturnPreviousScene();
            }

            StartCoroutine(Routine());
        }

        private void SaveVerification()
        {
            var lastLevel = SaveData.Current.highestLevelsUnlocked;
            switch (lastLevel)
            {
                case LevelsEnum.LEVEL_TUTORIAL:
                    levelOneButton.interactable = false;
                    levelTwoButton.interactable = false;
                    levelThreeButton.interactable = false;
                    levelFourButton.interactable = false;
                    break;

                case LevelsEnum.LEVEL_ONE:
                    levelTwoButton.interactable = false;
                    levelThreeButton.interactable = false;
                    levelFourButton.interactable = false;
                    break;

                case LevelsEnum.LEVEL_TWO:
                    levelThreeButton.interactable = false;
                    levelFourButton.interactable = false;
                    break;

                case LevelsEnum.LEVEL_THREE:
                    levelFourButton.interactable = false;
                    break;
            }
        }

        private void ToggleTutorial(bool isInteractable)
        {
            levelTutorialButton.interactable = isInteractable;
        }
    }
}