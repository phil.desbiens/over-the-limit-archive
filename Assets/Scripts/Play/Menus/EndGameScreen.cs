﻿using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class EndGameScreen : MonoBehaviour
    {
        private Main main;
        private Button quitButton;

        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            quitButton = buttons.WithName(GameObjects.Quitter);
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            quitButton.Select();
        }

        private void OnEnable()
        {
            quitButton.onClick.AddListener(OnQuitButtonClick);
        }

        private void OnDisable()
        {
            quitButton.onClick.RemoveListener(OnQuitButtonClick);
        }

        private void OnQuitButtonClick()
        {
            IEnumerator Routine()
            {
                TimeManager.ResumeGame();
                yield return main.ResetToMenuScene();
            }

            StartCoroutine(Routine());
        }
    }
}