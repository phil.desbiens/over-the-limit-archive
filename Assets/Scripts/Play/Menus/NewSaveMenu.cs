﻿using System.Collections;
using System.IO;
using Game;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Gabriel Fortier
    public class NewSaveMenu : MonoBehaviour
    {
        private Main main;
        private InputActions.MenuActions menuInputs;

        private Button saveOneButton;
        private Button saveTwoButton;
        private Button saveThreeButton;
        private Button returnButton;

        private const string SAVE_NAME_ONE = "SaveOne.save";
        private const string SAVE_NAME_TWO = "SaveTwo.save";
        private const string SAVE_NAME_THREE = "SaveThree.save";
        private const string OVERWRITE_ONE = "Overwrite save 1";
        private const string OVERWRITE_TWO = "Overwrite save 2";
        private const string OVERWRITE_THREE = "Overwrite save 3";

        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            saveOneButton = buttons.WithName(GameObjects.NewSaveOne);
            saveTwoButton = buttons.WithName(GameObjects.NewSaveTwo);
            saveThreeButton = buttons.WithName(GameObjects.NewSaveThree);
            returnButton = buttons.WithName(GameObjects.NewSaveReturn);

            SaveVerification();
        }

        private void Start()
        {
            saveOneButton.Select();
        }

        private void OnEnable()
        {
            saveOneButton.onClick.AddListener(NewSaveOne);
            saveTwoButton.onClick.AddListener(NewSaveTwo);
            saveThreeButton.onClick.AddListener(NewSaveThree);
            returnButton.onClick.AddListener(ReturnHomeScreen);
        }

        private void OnDisable()
        {
            saveOneButton.onClick.RemoveListener(NewSaveOne);
            saveTwoButton.onClick.RemoveListener(NewSaveTwo);
            saveThreeButton.onClick.RemoveListener(NewSaveThree);
            returnButton.onClick.RemoveListener(ReturnHomeScreen);
        }

        private void NewSaveOne()
        {
            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_ONE))
            {
                SaveManager.DeleteSave(Globals.PATH_SAVE_ONE);
            }

            SaveData.Current.saveName = SAVE_NAME_ONE;

            LoadNewGame();
        }

        private void NewSaveTwo()
        {
            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_TWO))
            {
                SaveManager.DeleteSave(Globals.PATH_SAVE_TWO);
            }
            
            SaveData.Current.saveName = SAVE_NAME_TWO;

            LoadNewGame();
        }

        private void NewSaveThree()
        {
            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_THREE))
            {
                SaveManager.DeleteSave(Globals.PATH_SAVE_THREE);
            }
            
            SaveData.Current.saveName = SAVE_NAME_THREE;

            LoadNewGame();
        }

        private void ReturnHomeScreen()
        {
            IEnumerator Routine()
            {
                yield return main.ReturnPreviousScene();
            }

            StartCoroutine(Routine());
        }

        private void SaveVerification()
        {
            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_ONE))
            {
                saveOneButton.GetComponentInChildren<TMP_Text>().text = OVERWRITE_ONE;
            }

            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_TWO))
            {
                saveTwoButton.GetComponentInChildren<TMP_Text>().text = OVERWRITE_TWO;
            }

            if (File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_THREE))
            {
                saveThreeButton.GetComponentInChildren<TMP_Text>().text = OVERWRITE_THREE;
            }
        }

        private void LoadNewGame()
        {
            IEnumerator Routine()
            {
                yield return main.LoadLevelSelectScene();
            }

            StartCoroutine(Routine());
        }
    }
}