﻿using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Gabriel Fortier
    public class OptionsMenu : MonoBehaviour
    {
        private readonly (int, int) regularWidthAndHeight = (1920, 1080);
        private readonly (int, int) mediumWidthAndHeight = (1440, 720);
        private readonly (int, int) smallWidthAndHeight = (1220, 720);

        private Main main;

        private Button regularScreenSizeButton;
        private Button mediumScreenSizeButton;
        private Button smallScreenSizeButton;
        private Button returnButton;


        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            regularScreenSizeButton = buttons.WithName(GameObjects.RegularSize);
            mediumScreenSizeButton = buttons.WithName(GameObjects.MediumSize);
            smallScreenSizeButton = buttons.WithName(GameObjects.SmallSize);
            returnButton = buttons.WithName(GameObjects.ReturnButton);
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            returnButton.Select();
        }

        private void OnEnable()
        {
            regularScreenSizeButton.onClick.AddListener(SetRegularSize);
            mediumScreenSizeButton.onClick.AddListener(SetMediumSize);
            smallScreenSizeButton.onClick.AddListener(SetSmallSize);
            returnButton.onClick.AddListener(QuitOptions);
        }

        private void OnDisable()
        {
            regularScreenSizeButton.onClick.RemoveListener(SetRegularSize);
            mediumScreenSizeButton.onClick.RemoveListener(SetMediumSize);
            smallScreenSizeButton.onClick.RemoveListener(SetSmallSize);
            returnButton.onClick.RemoveListener(QuitOptions);
        }

        private void SetRegularSize()
        {
            var (width, height) = regularWidthAndHeight;
            Screen.SetResolution(width, height, true);
        }

        private void SetMediumSize()
        {
            var (width, height) = mediumWidthAndHeight;
            Screen.SetResolution(width, height, true);
        }

        private void SetSmallSize()
        {
            var (width, height) = smallWidthAndHeight;
            Screen.SetResolution(width, height, true);
        }

        private void QuitOptions()
        {
            IEnumerator Routine()
            {
                yield return main.ReturnPreviousScene();
            }

            StartCoroutine(Routine());
        }
    }
}