﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class MenuStacker : MonoBehaviour
    {
        private readonly Stack<SceneBundle> sceneStack = new Stack<SceneBundle>();

        private SceneBundleLoader loader;

        private const string PAUSE = "Pause";

        private void Awake()
        {
            loader = Finder.SceneBundleLoader;
        }

        public Coroutine LoadNewSceneOnTop(SceneBundle bundle)
        {
            IEnumerator Routine()
            {
                yield return loader.Load(bundle);
                
                if (sceneStack.Count > 0 && bundle.Scenes[0].Name != PAUSE)
                    yield return loader.Unload(sceneStack.Peek());
                
                sceneStack.Push(bundle);
            }

            return StartCoroutine(Routine());
        }

        public Coroutine UnloadCurrentScene()
        {
            IEnumerator Routine()
            {
                yield return loader.Unload(sceneStack.Peek());
            }

            return StartCoroutine(Routine());
        }

        public Coroutine LoadCurrentScene()
        {
            IEnumerator Routine()
            {
                yield return loader.Load(sceneStack.Peek());
            }

            return StartCoroutine(Routine());
        }

        public Coroutine ReturnPreviousScene()
        {
            IEnumerator Routine()
            {
                var sceneToUnload = sceneStack.Pop();
                
                yield return loader.Load(sceneStack.Peek());
                yield return loader.Unload(sceneToUnload);
            }

            return StartCoroutine(Routine());
        }

        public Coroutine ReturnToFirstScene()
        {
            IEnumerator Routine()
            {
                while (sceneStack.Count > 1)
                {
                    yield return loader.Unload(sceneStack.Pop());
                }

                yield return loader.Load(sceneStack.Peek());
            }

            return StartCoroutine(Routine());
        }
    }
}