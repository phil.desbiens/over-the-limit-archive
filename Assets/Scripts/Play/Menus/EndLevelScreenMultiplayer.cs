﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Philippe Desbiens
    public class EndLevelScreenMultiplayer : MonoBehaviour
    {
        [SerializeField] private string levelTitleFormat = "Level {0:0}";
        [SerializeField] private string levelStatsScoreFormat = "Score :\t\t\t{00000} $";

        private Button quitToLevelSelectionButton;
        private Game game;

        private void Awake()
        {
            game = Finder.Game;

            Button[] buttons = GetComponentsInChildren<Button>();
            quitToLevelSelectionButton = buttons.WithName(GameObjects.QuitToLevelSelection);

            UpdateView();

            Finder.Main.EndLevelSound.Play();
        }

        private void Start()
        {
            quitToLevelSelectionButton.Select();
        }

        private void OnEnable()
        {
            quitToLevelSelectionButton.onClick.AddListener(OnQuitToLevelSelectionClick);
        }

        private void OnDisable()
        {
            quitToLevelSelectionButton.onClick.RemoveListener(OnQuitToLevelSelectionClick);
        }

        private void UpdateView()
        {
            var titleText = transform.Find(GameObjects.LevelTitle).GetComponent<TMP_Text>();
            titleText.text = string.Format(levelTitleFormat, (int) game.CurrentLevel);

            var player1Score = transform.Find(GameObjects.Player1Score).GetComponentInChildren<TMP_Text>();
            var player2Score = transform.Find(GameObjects.Player2Score).GetComponentInChildren<TMP_Text>();

            player1Score.text = string.Format(levelStatsScoreFormat, game.ShoppingCarts[0].MoneySpent);
            player2Score.text = string.Format(levelStatsScoreFormat, game.ShoppingCarts[1].MoneySpent);
        }

        private void OnQuitToLevelSelectionClick()
        {
            game.EndLevel();
        }
    }
}