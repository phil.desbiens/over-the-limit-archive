﻿using System.Collections.Generic;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class LevelEntryScreen : MonoBehaviour
    {
        [SerializeField] private List<ItemNameEnum> itemEnums = new List<ItemNameEnum>();
        [SerializeField] private string levelTitleFormat = "Level {0:0}";

        private const string TUTORIAL_LEVEL_NAME = "Tutorial";

        private Game game;
        private ItemsFactory factory;
        private Button startButton;

        private void Awake()
        {
            game = Finder.Game;
            factory = Finder.ItemsFactory;

            Button[] buttons = GetComponentsInChildren<Button>();

            startButton = buttons.WithName(GameObjects.Commencer);

            SetItemsPreview();

            Finder.Main.MenuMusic.Stop();
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            startButton.Select();
        }

        private void OnEnable()
        {
            startButton.onClick.AddListener(OnStartLevelButtonClick);
        }

        private void OnDisable()
        {
            startButton.onClick.RemoveListener(OnStartLevelButtonClick);
        }

        private void SetItemsPreview()
        {
            var titleText = transform.Find(GameObjects.LevelTitle).GetComponent<TMP_Text>();

            titleText.text = game.CurrentLevel != LevelsEnum.LEVEL_TUTORIAL
                ? string.Format(levelTitleFormat, (int) game.CurrentLevel)
                : string.Format(levelTitleFormat, TUTORIAL_LEVEL_NAME);

            var itemPreviews = transform.Find(GameObjects.ItemPreviews);
            var itemImages = itemPreviews.GetComponentsInChildren<Image>();
            var itemTexts = itemPreviews.GetComponentsInChildren<TMP_Text>();

            for (var i = 0;
                i < itemImages.Length
                && i + itemImages.Length * (int) game.CurrentLevel < itemEnums.Count;
                i++)
            {
                var item =
                    factory.GetItemToSpawn(itemEnums[i + itemImages.Length * (int) game.CurrentLevel]);
                itemImages[i].sprite = item.GetComponent<SpriteRenderer>().sprite;
                itemTexts[i].text = item.GetComponentInChildren<TMP_Text>().text;
            }
        }

        private void OnStartLevelButtonClick()
        {
            game.StartLevel();
        }
    }
}