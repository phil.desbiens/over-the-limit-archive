﻿using System.Collections;
using System.IO;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Gabriel Fortier
    public class SaveMenu : MonoBehaviour
    {
        private const string NO_SAVE = "No save";
        
        private Main main;
        private Button returnButton;
        private Button saveOneButton;
        private Button saveThreeButton;
        private Button saveTwoButton;

        private void Awake()
        {
            main = Finder.Main;

            Button[] buttons = GetComponentsInChildren<Button>();
            saveOneButton = buttons.WithName(GameObjects.SaveOne);
            saveTwoButton = buttons.WithName(GameObjects.SaveTwo);
            saveThreeButton = buttons.WithName(GameObjects.SaveThree);
            returnButton = buttons.WithName(GameObjects.Return);

            SaveVerification();
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            saveOneButton.Select();
        }

        private void OnEnable()
        {
            saveOneButton.onClick.AddListener(LoadSaveOne);
            saveTwoButton.onClick.AddListener(LoadSaveTwo);
            saveThreeButton.onClick.AddListener(LoadSaveThree);
            returnButton.onClick.AddListener(ReturnHomeScreen);
        }

        private void OnDisable()
        {
            saveOneButton.onClick.RemoveListener(LoadSaveOne);
            saveTwoButton.onClick.RemoveListener(LoadSaveTwo);
            saveThreeButton.onClick.RemoveListener(LoadSaveThree);
            returnButton.onClick.RemoveListener(ReturnHomeScreen);
        }

        private void LoadSaveOne()
        {
            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_ONE)) return;

            SaveData.Current = SaveManager.LoadSave(Globals.PATH_SAVE_ONE);

            LoadGame();
        }

        private void LoadSaveTwo()
        {
            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_TWO)) return;

            SaveData.Current = SaveManager.LoadSave(Globals.PATH_SAVE_TWO);

            LoadGame();
        }

        private void LoadSaveThree()
        {
            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_THREE)) return;

            SaveData.Current = SaveManager.LoadSave(Globals.PATH_SAVE_THREE);

            LoadGame();
        }

        private void ReturnHomeScreen()
        {
            IEnumerator Routine()
            {
                yield return main.ReturnPreviousScene();
            }

            StartCoroutine(Routine());
        }

        private void SaveVerification()
        {
            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_ONE))
            {
                saveOneButton.interactable = false;
                saveOneButton.GetComponentInChildren<TMP_Text>().text = NO_SAVE;
            }

            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_TWO))
            {
                saveTwoButton.interactable = false;
                saveTwoButton.GetComponentInChildren<TMP_Text>().text = NO_SAVE;
            }

            if (!File.Exists(Application.persistentDataPath + Globals.PATH_SAVE_THREE))
            {
                saveThreeButton.interactable = false;
                saveThreeButton.GetComponentInChildren<TMP_Text>().text = NO_SAVE;
            }
        }

        private void LoadGame()
        {
            IEnumerator Routine()
            {
                yield return main.LoadLevelSelectScene();
            }

            StartCoroutine(Routine());
        }
    }
}