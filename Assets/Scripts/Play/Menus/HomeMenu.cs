using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Gabriel Fortier
    public class HomeMenu : MonoBehaviour
    {
        private Main main;
        private Button newGameButton;
        private Button optionsButton;
        private Button playButton;
        private Button quitButton;

        private void Awake()
        {
            main = Finder.Main;
            Button[] buttons = GetComponentsInChildren<Button>();
            playButton = buttons.WithName(GameObjects.Continue);
            newGameButton = buttons.WithName(GameObjects.NewGame);
            optionsButton = buttons.WithName(GameObjects.Options);
            quitButton = buttons.WithName(GameObjects.Quit);
            
            SaveData.Current = new SaveData();
        }

        // Author : Philippe Desbiens
        private void Start()
        {
            playButton.Select();
        }

        private void OnEnable()
        {
            playButton.onClick.AddListener(StartGame);
            newGameButton.onClick.AddListener(StartNewGame);
            optionsButton.onClick.AddListener(OpenOptions);
            quitButton.onClick.AddListener(QuitApplication);
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveListener(StartGame);
            newGameButton.onClick.RemoveListener(StartNewGame);
            optionsButton.onClick.RemoveListener(OpenOptions);
            quitButton.onClick.RemoveListener(QuitApplication);
        }

        private void StartGame()
        {
            IEnumerator Routine()
            {
                yield return main.LoadSaveScene();
            }

            StartCoroutine(Routine());
        }

        private static void QuitApplication()
        {
            ApplicationExtensions.Quit();
        }

        private void StartNewGame()
        {
            IEnumerator Routine()
            {
                yield return main.LoadNewSaveScene();
            }

            StartCoroutine(Routine());
        }

        private void OpenOptions()
        {
            IEnumerator Routine()
            {
                yield return main.LoadOptionsScenes();
            }

            StartCoroutine(Routine());
        }
    }
}