﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Kaylen Bélanger
    public class PlatformSensor : MonoBehaviour
    {
        private void OnCollisionStay2D(Collision2D other)
        {
            var platform = other.gameObject.GetComponent<Platform>();
            if (platform != null) OnPlatformSensed?.Invoke(platform);
        }

        public event EventHandler<Platform> OnPlatformSensed;
    }
}