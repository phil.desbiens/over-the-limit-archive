﻿using Harmony;
using UnityEngine;

namespace Play
{
    // Author : Gabriel Fortier
    public class PlayerSensor : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var player = other.GetComponent<PlayerController>();
            if (player != null) OnPlayerSensed?.Invoke(player);
        }

        public event EventHandler<PlayerController> OnPlayerSensed;
    }
}