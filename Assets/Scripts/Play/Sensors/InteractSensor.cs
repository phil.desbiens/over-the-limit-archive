﻿using UnityEngine;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    public class InteractSensor : MonoBehaviour
    {
        private PlayerInteractor playerInteractor;

        private void Awake()
        {
            playerInteractor = GetComponent<PlayerInteractor>();
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            var interactable = other.gameObject.GetComponent<IInteractable>();
            if (interactable != null) playerInteractor.NearestInteractable = interactable;

            if (other.gameObject.GetComponent<ItemEntity>() != null
                && (other.gameObject.GetComponent<ItemEntity>().Name == ItemNameEnum.DISCOUNT_TAG
                    || other.gameObject.GetComponent<ItemEntity>().Name == ItemNameEnum.DISCOUNT_TAG_LEVEL_THREE))
            {
                playerInteractor.NearestInteractable.Interact(GetComponent<PlayerController>());
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            playerInteractor.NearestInteractable = null;
        }
    }
}