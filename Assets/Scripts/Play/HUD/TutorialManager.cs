﻿using System.Collections;
using Harmony;
using UnityEngine;

// Author : Kaylen Bélanger
namespace Play
{
    public class TutorialManager : MonoBehaviour
    {
        [Header("Time allocated for the tutorial")] [SerializeField] [Min(0)]
        private float tutorialTime = 1000f;

        [Header("Height of the ladder")] [SerializeField] [Min(0)]
        private float ladderHeight = 2.5f;

        private Game game;
        private GameObject[] itemSpawners;
        private GameObject karenSpawner;
        private GameObject player;
        private GameObject discountTagSpawner;
        private Vector2 playerPositionBeforeTooltip;
        private ShoppingCart shoppingCart;

        private GameObject movementTooltip;
        private GameObject jumpTooltip;
        private GameObject ladderTooltip;
        private GameObject buySellTooltip;
        private GameObject karenTooltip;

        private void OnEnable()
        {
            itemSpawners = GameObject.FindGameObjectsWithTag(Tags.ItemSpawner);
            karenSpawner = GameObject.FindGameObjectWithTag(Tags.KarenSpawner);
            discountTagSpawner = GameObject.FindWithTag(Tags.DiscountTagSpawner);
            game = Finder.Game;

            movementTooltip = transform.Find(GameObjects.SidewaysMovementTutorial).gameObject;
            jumpTooltip = transform.Find(GameObjects.JumpTutorial).gameObject;
            ladderTooltip = transform.Find(GameObjects.LadderTutorial).gameObject;
            buySellTooltip = transform.Find(GameObjects.BuySellTutorial).gameObject;
            karenTooltip = transform.Find(GameObjects.KarenTutorial).gameObject;

            StartCoroutine(Tutorial());
        }

        IEnumerator Tutorial()
        {
            yield return InitTutorial();
            yield return HorizontalMovementTutorial();
            yield return JumpTutorial();
            yield return LadderTutorial();
            yield return ItemPickUpTutorial();
            yield return KarenTutorial();

            StartLevel();
        }

        private IEnumerator HorizontalMovementTutorial()
        {
            movementTooltip.SetActive(true);
            SetCurrentPlayerPosition();

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            yield return new WaitUntil(() => player.transform.position.x != playerPositionBeforeTooltip.x);
            movementTooltip.SetActive(false);
        }

        private IEnumerator JumpTutorial()
        {
            jumpTooltip.SetActive(true);
            SetCurrentPlayerPosition();
            yield return new WaitUntil(() => player.transform.position.y > playerPositionBeforeTooltip.y);
            jumpTooltip.SetActive(false);
        }

        private IEnumerator LadderTutorial()
        {
            ladderTooltip.SetActive(true);
            SetCurrentPlayerPosition();
            yield return new WaitUntil(() =>
                player.transform.position.y - playerPositionBeforeTooltip.y > ladderHeight);
            int randomItemSelector = Finder.Random.Next(itemSpawners.Length);
            itemSpawners[randomItemSelector].SetActive(true);
            ladderTooltip.SetActive(false);
        }

        private IEnumerator ItemPickUpTutorial()
        {
            buySellTooltip.SetActive(true);
            shoppingCart = player.GetComponent<ShoppingCart>();
            yield return new WaitUntil(() => shoppingCart.ShoppingCartValue != 0);
            buySellTooltip.SetActive(false);
        }

        private IEnumerator KarenTutorial()
        {
            karenTooltip.SetActive(true);
            SetCurrentPlayerPosition();
            yield return new WaitUntil(() => shoppingCart.ShoppingCartValue == 0);
            karenTooltip.SetActive(false);
        }

        private void SetCurrentPlayerPosition()
        {
            playerPositionBeforeTooltip = player.transform.position;
        }

        private IEnumerator InitTutorial()
        {
            DisableAll();
            game.SetTimeRemaining(tutorialTime);
            yield return null;

            player = GameObject.FindWithTag(Tags.Player);
        }

        private void StartLevel()
        {
            EnableAll();
            game.ResetTimeToDefault();
        }

        private void EnableAll()
        {
            foreach (var spawners in itemSpawners)
            {
                spawners.SetActive(true);
            }

            discountTagSpawner.SetActive(true);
            karenSpawner.SetActive(true);
        }

        private void DisableAll()
        {
            foreach (var spawners in itemSpawners)
            {
                spawners.SetActive(false);
            }

            movementTooltip.SetActive(false);
            jumpTooltip.SetActive(false);
            ladderTooltip.SetActive(false);
            buySellTooltip.SetActive(false);
            karenTooltip.SetActive(false);

            karenSpawner.SetActive(false);
            discountTagSpawner.SetActive(false);
        }
    }
}