﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Play
{
    // Author : Zachary Derosby-Therrien
    // HUD is an abbreviation 
    // ReSharper disable once InconsistentNaming
    public class HUDController : MonoBehaviour
    {
        [SerializeField] private string moneySpentFormat = "{0:0} $ / {1:0}";
        [SerializeField] private string moneySpentFormatMultiplayer = "{0:0} $";
        [SerializeField] private string shoppingCartFormat = "{0:0} $";
        [SerializeField] private string timeFormat = "{0:0}:{1:00}";

        private Game game;
        private Main main;

        private PlayerPayEventChannel playerPayEventChannel;
        private PlayerPayEventChannel playerPayEventChannel2;
        private ShoppingCartEventChannel shoppingCartEventChannel;
        private ShoppingCartEventChannel shoppingCartEventChannel2;

        private Image separator;
        private TMP_Text currentMoneySpent1Text;
        private TMP_Text currentMoneySpent2Text;
        private TMP_Text shoppingCart1ValueText;
        private TMP_Text shoppingCart2ValueText;
        private TMP_Text timeText;

        private void Awake()
        {
            game = Finder.Game;
            main = Finder.Main;

            TMP_Text[] uiText = GetComponentsInChildren<TMP_Text>();
            currentMoneySpent1Text = uiText.WithName(GameObjects.MoneySpent);
            currentMoneySpent2Text = uiText.WithName(GameObjects.MoneySpent2);
            shoppingCart1ValueText = uiText.WithName(GameObjects.ShoppingCartValue);
            shoppingCart2ValueText = uiText.WithName(GameObjects.ShoppingCart2Value);
            timeText = uiText.WithName(GameObjects.Time);

            separator = GetComponentInChildren<Image>();

            var player = GameObject.FindGameObjectWithTag(Tags.Player);
            playerPayEventChannel = player.GetComponent<PlayerPayEventChannel>();
            shoppingCartEventChannel = player.GetComponent<ShoppingCartEventChannel>();

            shoppingCartEventChannel.OnShoppingCartValueUpdate += OnShoppingCartValue;
            playerPayEventChannel.OnPlayerPay += OnPlayerPay;

            if (main.GameMode == GameModesEnum.PVP)
            {
                var player2 = GameObject.FindGameObjectWithTag(Tags.Player2);

                playerPayEventChannel2 = player2.GetComponent<PlayerPayEventChannel>();
                shoppingCartEventChannel2 = player2.GetComponent<ShoppingCartEventChannel>();

                shoppingCartEventChannel2.OnShoppingCartValueUpdate += OnShoppingCartValue;
                playerPayEventChannel2.OnPlayerPay += OnPlayerPay;
            }
            else
            {
                separator.color = new Color(0, 0, 0, 0);
                shoppingCart2ValueText.alpha = 0f;
                currentMoneySpent2Text.alpha = 0f;
            }

            UpdateCurrentMoneySpent();
            UpdateShoppingCartValue();
        }

        private void Update()
        {
            UpdateTime(game.TimeRemaining);
        }

        private void OnDestroy()
        {
            shoppingCartEventChannel.OnShoppingCartValueUpdate -= OnShoppingCartValue;
            playerPayEventChannel.OnPlayerPay -= OnPlayerPay;

            if (main.GameMode == GameModesEnum.PVP)
            {
                shoppingCartEventChannel2.OnShoppingCartValueUpdate -= OnShoppingCartValue;
                playerPayEventChannel2.OnPlayerPay -= OnPlayerPay;
            }
        }

        private void OnShoppingCartValue()
        {
            UpdateShoppingCartValue();
        }

        private void OnPlayerPay()
        {
            UpdateCurrentMoneySpent();
        }

        private void UpdateCurrentMoneySpent()
        {
            string formatString = moneySpentFormat;

            if (game.ShoppingCarts != null)
            {
                if (main.GameMode == GameModesEnum.PVP)
                {
                    formatString = moneySpentFormatMultiplayer;
                }

                short playerNumber = 0;

                currentMoneySpent1Text.text =
                    formatString.Format(game.ShoppingCarts[playerNumber].MoneySpent, game.SpendingGoal);

                playerNumber++;

                if (main.GameMode == GameModesEnum.PVP)
                {
                    currentMoneySpent2Text.text =
                        formatString.Format(game.ShoppingCarts[playerNumber].MoneySpent, game.SpendingGoal);
                }
            }
            else currentMoneySpent1Text.text = formatString.Format(0, game.SpendingGoal);
        }

        private void UpdateShoppingCartValue()
        {
            if (game.ShoppingCarts != null)
            {
                short playerNumber = 0;

                shoppingCart1ValueText.text =
                    shoppingCartFormat.Format(game.ShoppingCarts[playerNumber].ShoppingCartValue);

                playerNumber++;

                if (main.GameMode == GameModesEnum.PVP)
                {
                    shoppingCart2ValueText.text =
                        shoppingCartFormat.Format(game.ShoppingCarts[playerNumber].ShoppingCartValue);
                }
            }
            else shoppingCart1ValueText.text = shoppingCartFormat.Format(0);
        }

        // Author : Gabriel Fortier
        private void UpdateTime(float timeRemaining)
        {
            timeRemaining += 1;
            float minutes = Mathf.FloorToInt(timeRemaining / 60);
            float seconds = Mathf.FloorToInt(timeRemaining % 60);

            timeText.text = string.Format(timeFormat, minutes, seconds);
        }
    }
}