﻿namespace Play
{
    // Author : Zachary Derosby-Therrien
    public enum LevelsEnum
    {
        LEVEL_TUTORIAL = 0,
        LEVEL_ONE = 1,
        LEVEL_TWO = 2,
        LEVEL_THREE = 3,
        LEVEL_FOUR = 4,
        GAME_END = 5
    }
}