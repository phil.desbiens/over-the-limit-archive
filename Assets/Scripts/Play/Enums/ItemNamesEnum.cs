﻿namespace Play
{
    // Author : Zachary Derosby-Therrien
    public enum ItemNameEnum
    {
        BREAD = 0,
        COOKIE = 1,
        JAM = 2,
        BROWNIE = 3,
        PIE = 4,
        JERKY = 5,
        CHICKEN_LEG = 6,
        RIBS = 7,
        BACON = 8,
        STEAK = 9,
        SHIELD = 10,
        HELMET = 11,
        GAUNTLET = 12,
        RING = 13,
        CHEST_PLATE = 14,
        RED_POTION = 15,
        BLUE_POTION = 16,
        PURPLE_POTION = 17,
        GREEN_POTION = 18,
        WHITE_POTION = 19,
        DAGGER = 20,
        BOW = 21,
        SWORD = 22,
        MACE = 23,
        BATTLEAXE = 24,
        DISCOUNT_TAG = 25,
        DISCOUNT_TAG_LEVEL_THREE = 26
    }
}