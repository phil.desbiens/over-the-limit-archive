﻿namespace Play
{
    // Author : Guillaume Thivierge
    public enum AchievementsEnum
    {
        WEEB_ARSENAL = 0,
        HIGH_ON_PURCHASE = 1,
        CAPITAL_TWO = 2,
        RED_CARD = 3,
        SAFE_RIDE = 4,
        GET_IT_WHILE_ITS_HOT = 5,
        BAGUETTE = 6,
        POTION_SELLER = 7
    }
}