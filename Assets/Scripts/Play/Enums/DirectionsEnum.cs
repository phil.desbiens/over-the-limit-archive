﻿namespace Play
{
    // Author : Philippe Desbiens
    public enum DirectionsEnum
    {
        LEFT = -1,
        NONE = 0,
        RIGHT = 1
    }
}